
    /**
     * Stops the creature moving in the passed direction
     */import {inject, TestBed} from '@angular/core/testing';

import {InputService} from './input.service';
import {EventQueueService} from '../events/event-queue/event-queue.service';
import {Location} from '../map/location';
import {MovementAction} from './events/enums/movement-action.enum';
import {Vector} from '../../import/vector/vector';
import {MovementEvent} from './events/movement-event';
import {MousePositionUpdateEvent} from './events/mouse-position-update-event';
import {CreatureFactory} from '../map-object/creature/factory/creature.factory';
import {MouseClickEvent} from './events/mouse-click-event';
import {MouseButtonTypes} from './events/enums/mouse-button-types.enum';
import { DebugService } from '../services/debug/debug.service';

describe('InputService', () => {
    let creatureFactory;
    let player;
    let service;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {provide: DebugService, useValue: {printMsg: function () {}}},
                InputService
            ]
        });

        creatureFactory = new CreatureFactory();
        player = creatureFactory.getPlayer(new Location(10, 10), new Vector(0, 1));
    });

    beforeEach(inject([InputService], (ss) => {
        service = ss;
    }));

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should correctly handle key down events', () => {
        const eventSpy = spyOn(TestBed.get(EventQueueService), 'addEvent');

        const keyEventUp = new KeyboardEvent('testevent', {
            code: 'KeyW'
        });

        const keyEventDown = new KeyboardEvent('testevent', {
            code: 'KeyS'
        });

        const keyEventLeft = new KeyboardEvent('testevent', {
            code: 'KeyA'
        });

        const keyEventRight = new KeyboardEvent('testevent', {
            code: 'KeyD'
        });

        const keyEventNone = new KeyboardEvent('testevent', {
            code: 'KeyX'
        });

        service.handleKeyDownEvent(keyEventUp);
        expect(eventSpy).toHaveBeenCalledWith(new MovementEvent(null, MovementAction.START_MOVE_UP));
        service.handleKeyDownEvent(keyEventDown);
        expect(eventSpy).toHaveBeenCalledWith(new MovementEvent(null, MovementAction.START_MOVE_DOWN));
        service.handleKeyDownEvent(keyEventLeft);
        expect(eventSpy).toHaveBeenCalledWith(new MovementEvent(null, MovementAction.START_MOVE_LEFT));
        service.handleKeyDownEvent(keyEventRight);
        expect(eventSpy).toHaveBeenCalledWith(new MovementEvent(null, MovementAction.START_MOVE_RIGHT));
        service.handleKeyDownEvent(keyEventNone);
        expect(eventSpy).toHaveBeenCalledWith(new MovementEvent(null, MovementAction.NONE));

        expect(eventSpy).toHaveBeenCalledTimes(5);
    });

    it('should correctly handle key up events', () => {
        const eventSpy = spyOn(TestBed.get(EventQueueService), 'addEvent');

        const keyEventUp = new KeyboardEvent('testevent', {
            code: 'KeyW'
        });

        const keyEventDown = new KeyboardEvent('testevent', {
            code: 'KeyS'
        });

        const keyEventLeft = new KeyboardEvent('testevent', {
            code: 'KeyA'
        });

        const keyEventRight = new KeyboardEvent('testevent', {
            code: 'KeyD'
        });

        const keyEventNone = new KeyboardEvent('testevent', {
            code: 'KeyX'
        });

        service.handleKeyUpEvent(keyEventUp);
        expect(eventSpy).toHaveBeenCalledWith(new MovementEvent(null, MovementAction.END_MOVE_UP));
        service.handleKeyUpEvent(keyEventDown);
        expect(eventSpy).toHaveBeenCalledWith(new MovementEvent(null, MovementAction.END_MOVE_DOWN));
        service.handleKeyUpEvent(keyEventLeft);
        expect(eventSpy).toHaveBeenCalledWith(new MovementEvent(null, MovementAction.END_MOVE_LEFT));
        service.handleKeyUpEvent(keyEventRight);
        expect(eventSpy).toHaveBeenCalledWith(new MovementEvent(null, MovementAction.END_MOVE_RIGHT));
        service.handleKeyUpEvent(keyEventNone);
        expect(eventSpy).toHaveBeenCalledWith(new MovementEvent(null, MovementAction.NONE));

        expect(eventSpy).toHaveBeenCalledTimes(5);
    });

    it('should correctly handle mouse move events', () => {
        const eventSpy = spyOn(TestBed.get(EventQueueService), 'addEvent');
        const mouseEvent = new MouseEvent('testEvent', {
            clientX: 10,
            clientY: 10
        });

        service.handleMouseMoveEvent(mouseEvent);

        expect(eventSpy).toHaveBeenCalledTimes(1);
        expect(eventSpy).toHaveBeenCalledWith(new MousePositionUpdateEvent(null, new Location(10, 10)));
    });

    it('should correctly handle mouse click events', () => {
        const eventSpy = spyOn(TestBed.get(EventQueueService), 'addEvent');
        let mouseEvent = new MouseEvent('testEvent', {
            button: 0
        });

        service.handleMouseClickEvent(mouseEvent);

        expect(eventSpy).toHaveBeenCalledTimes(1);
        expect(eventSpy).toHaveBeenCalledWith(new MouseClickEvent(null, MouseButtonTypes.LEFT_CLICK));

        mouseEvent = new MouseEvent('testEvent', {
            button: 1
        });
        service.handleMouseClickEvent(mouseEvent);

        expect(eventSpy).toHaveBeenCalledTimes(2);
        expect(eventSpy).toHaveBeenCalledWith(new MouseClickEvent(null, MouseButtonTypes.MIDDLE_CLICK));

        mouseEvent = new MouseEvent('testEvent', {
            button: 2
        });
        service.handleMouseClickEvent(mouseEvent);

        expect(eventSpy).toHaveBeenCalledTimes(3);
        expect(eventSpy).toHaveBeenCalledWith(new MouseClickEvent(null, MouseButtonTypes.RIGHT_CLICK));
    });
});
