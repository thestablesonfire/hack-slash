import {Event} from './event';
import {MovementAction} from './enums/movement-action.enum';
import {Creature} from '../../map-object/creature/creature';

export class MovementEvent extends Event {
    constructor(public target: Creature, public action: MovementAction) {
        super(target);
    }
}
