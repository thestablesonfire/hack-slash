import {Event} from './event';
import {MapObject} from '../../map-object/map-object';
import {MouseButtonTypes} from './enums/mouse-button-types.enum';

export class MouseClickEvent extends Event {
    constructor(public target: MapObject, public button: MouseButtonTypes) {
        super(target);
    }
}
