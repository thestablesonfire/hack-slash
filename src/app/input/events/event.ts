import {MapObject} from '../../map-object/map-object';

export class Event {
    constructor(public target: MapObject) {}
}
