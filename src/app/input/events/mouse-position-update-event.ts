import {Event} from './event';
import {Location} from '../../map/location';
import {MapObject} from '../../map-object/map-object';

export class MousePositionUpdateEvent extends Event {
    constructor(public target: MapObject, public position: Location) {
        super(target);
    }
}
