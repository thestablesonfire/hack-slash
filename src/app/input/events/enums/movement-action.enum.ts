export enum MovementAction {
    NONE = 'None',
    END_MOVE_UP = 'End Move Up',
    END_MOVE_DOWN = 'End Move Down',
    END_MOVE_LEFT = 'End Move Left',
    END_MOVE_RIGHT = 'End Move Right',
    START_MOVE_UP = 'Start Move Up',
    START_MOVE_DOWN = 'Start Move Down',
    START_MOVE_LEFT = 'Start Move Left',
    START_MOVE_RIGHT = 'Start Move Right',
}
