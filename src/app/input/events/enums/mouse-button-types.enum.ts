export enum MouseButtonTypes {
    LEFT_CLICK = 'Left Click',
    MIDDLE_CLICK = 'Middle Click',
    RIGHT_CLICK = 'Right Click'
}
