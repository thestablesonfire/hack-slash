export enum KeyCodes {
    MOVE_UP = 'KeyW',
    MOVE_DOWN = 'KeyS',
    MOVE_LEFT = 'KeyA',
    MOVE_RIGHT = 'KeyD'
}
