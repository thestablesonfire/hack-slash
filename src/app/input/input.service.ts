import {Injectable} from '@angular/core';
import {KeyCodes} from './events/enums/key-codes.enum';
import {EventQueueService} from '../events/event-queue/event-queue.service';
import {MovementAction} from './events/enums/movement-action.enum';
import {MovementEvent} from './events/movement-event';
import {MousePositionUpdateEvent} from './events/mouse-position-update-event';
import {Location} from '../map/location';
import {MouseClickEvent} from './events/mouse-click-event';
import {MouseButtonTypes} from './events/enums/mouse-button-types.enum';

@Injectable({
    providedIn: 'root'
})
export class InputService {
    /**
     * Handles all input events from the user
     * @param eventQueue
     */
    constructor(
        private eventQueue: EventQueueService
    ) {}

    /**
     * Handles a KeyDown event
     * @param event
     */
    handleKeyDownEvent(event: KeyboardEvent) {
        let playerEvent;
        const player = null;

        switch (event.code) {
            case KeyCodes.MOVE_UP:
                playerEvent = new MovementEvent(player, MovementAction.START_MOVE_UP);
                break;
            case KeyCodes.MOVE_DOWN:
                playerEvent = new MovementEvent(player, MovementAction.START_MOVE_DOWN);
                break;
            case KeyCodes.MOVE_LEFT:
                playerEvent = new MovementEvent(player, MovementAction.START_MOVE_LEFT);
                break;
            case KeyCodes.MOVE_RIGHT:
                playerEvent = new MovementEvent(player, MovementAction.START_MOVE_RIGHT);
                break;
            default:
                playerEvent = new MovementEvent(player, MovementAction.NONE);
                break;
        }

        this.eventQueue.addEvent(playerEvent);
    }

    /**
     * Handles a KeyUp event
     * @param event
     */
    handleKeyUpEvent(event: KeyboardEvent) {
        let playerEvent;
        const player = null;

        switch (event.code) {
            case KeyCodes.MOVE_UP:
                playerEvent = new MovementEvent(player, MovementAction.END_MOVE_UP);
                break;
            case KeyCodes.MOVE_DOWN:
                playerEvent = new MovementEvent(player, MovementAction.END_MOVE_DOWN);
                break;
            case KeyCodes.MOVE_LEFT:
                playerEvent = new MovementEvent(player, MovementAction.END_MOVE_LEFT);
                break;
            case KeyCodes.MOVE_RIGHT:
                playerEvent = new MovementEvent(player, MovementAction.END_MOVE_RIGHT);
                break;
            default:
                playerEvent = new MovementEvent(player, MovementAction.NONE);
                break;
        }

        this.eventQueue.addEvent(playerEvent);
    }

    /**
     * Handles a MouseClick event
     * @param event
     */
    handleMouseClickEvent(event: MouseEvent) {
        let buttonType;

        switch (event.button) {
            case 0:
                buttonType = MouseButtonTypes.LEFT_CLICK;
                break;
            case 1:
                buttonType = MouseButtonTypes.MIDDLE_CLICK;
                break;
            case 2:
                buttonType = MouseButtonTypes.RIGHT_CLICK;
                break;
        }

        this.eventQueue.addEvent(
            new MouseClickEvent(null, buttonType)
        );
    }

    /**
     * Handles a MouseMove event
     * @param event
     */
    handleMouseMoveEvent(event: MouseEvent) {
        this.eventQueue.addEvent(
            new MousePositionUpdateEvent(
                null,
                new Location(event.clientX, event.clientY)
            )
        );
    }
}
