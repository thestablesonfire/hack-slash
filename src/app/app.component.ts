import {AfterViewInit, Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import {InputService} from './input/input.service';
import {CanvasDrawerService} from './map/canvasDrawer/canvas-drawer.service';
import {EventQueueService} from './events/event-queue/event-queue.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
    @ViewChild('displayCanvas') displayCanvas: ElementRef;
    isRunning: boolean;
    title = 'hack/slash';

    /**
     * Main application component
     * @param canvasDrawer
     * @param eventQueue
     * @param inputService
     */
    constructor(
        private canvasDrawer: CanvasDrawerService,
        private eventQueue: EventQueueService,
        private inputService: InputService
    ) {
        this.isRunning = true;
    }

    /**
     * Handles keyDown events
     * @param event
     */
    @HostListener('window:keydown', ['$event'])
    keyDown(event: KeyboardEvent) {
        this.inputService.handleKeyDownEvent(event);
    }

    /**
     * Handles keyUp events
     * @param event
     */
    @HostListener('window:keyup', ['$event'])
    keyUp(event: KeyboardEvent) {
        this.inputService.handleKeyUpEvent(event);
    }

    /**
     * Handles mouse click events
     * @param event
     */
    @HostListener('window:click', ['$event'])
    mouseClick(event: MouseEvent) {
        this.inputService.handleMouseClickEvent(event);
    }

    /**
     * Handles mouse move events
     * @param event
     */
    @HostListener('window:mousemove', ['$event'])
    mouseMove(event: MouseEvent) {
        this.inputService.handleMouseMoveEvent(event);
    }

    /**
     * Init
     */
    ngAfterViewInit() {
        this.canvasDrawer.setCanvas(this.displayCanvas.nativeElement);
        this.eventQueue.init();
    }

    /**
     * Manually starts the event queue loop
     */
    startLoop() {
        this.isRunning = true;
        this.eventQueue.startLoop();
    }

    /**
     * Manually stops the event queue loop
     */
    stopLoop() {
        this.isRunning = false;
        this.eventQueue.stopLoop();
    }
}
