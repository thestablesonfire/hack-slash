import { Injectable } from '@angular/core';
import {MapService} from '../../map/map.service';
import {Creature} from '../creature/creature';
import {Direction} from '../../map/direction.enum';
import {Vector} from '../../../import/vector/vector';
import {Location} from '../../map/location';
import {Player} from '../creature/player/player';
import {CollisionService} from '../service/collision.service';
import { MovementAction } from '../../input/events/enums/movement-action.enum';
import { CanvasGlobalsService } from '../../canvas-globals.service';
import { PathfinderService } from '../../services/pathfinder/pathfinder.service';
import { BoundingBox } from '../creature/bounding-box';
import { MathService } from 'src/app/services/math/math.service';

@Injectable({
    providedIn: 'root'
})
export class MovementService {
    private mousePosition: Location;

    /**
     * The service that handles the movement of creatures
     * @param mapService
     */
    constructor(
        private canvasGlobals: CanvasGlobalsService,
        private mapService: MapService,
        private pathfinder: PathfinderService
    ) {
        this.mousePosition = new Location(100, 100);
    }

    setPaths(creatures: Creature[], mapObjects: BoundingBox[]) {
        const player = creatures[0];

        if (player instanceof Player) {
            for (const creature of creatures) {
                if (player && creature !== player) {
                    this.setPath(creature, player, creatures);
                }
            }
        }
    }

    /**
     * Move, detects collisions, and updates facings for each creature
     */
    moveCreatures(creatures: Creature[]) {
        let player;

        if (creatures[0] instanceof Player) {
            player = creatures[0];
        }

        for (const creature of creatures) {
            this.moveCreature(creature);
            this.checkForCreatureCollision(creature, creatures);
            this.updateCreatureFacing(creature, player);
        }
    }

    /**
     * Starts or stops a movement in a passed direction
     */
    directionAction(target: Creature, action: MovementAction) {
        switch (action) {
            case MovementAction.END_MOVE_UP:
                this.stopUp(target);
                break;
            case MovementAction.END_MOVE_DOWN:
                this.stopDown(target);
                break;
            case MovementAction.END_MOVE_LEFT:
                this.stopLeft(target);
                break;
            case MovementAction.END_MOVE_RIGHT:
                this.stopRight(target);
                break;
            case MovementAction.START_MOVE_UP:
                this.startUp(target);
                break;
            case MovementAction.START_MOVE_DOWN:
                this.startDown(target);
                break;
            case MovementAction.START_MOVE_LEFT:
                this.startLeft(target);
                break;
            case MovementAction.START_MOVE_RIGHT:
                this.startRight(target);
                break;
        }
    }

    /**
     * Sets the path for the passed creature
     */
    setPath(creature: Creature, player: Player, creatures: Creature[]) {
        let path = this.pathfinder.findPath(creature, player, creatures);

        if (path.length) {
            creature.setPath(path);
        } else {
            path = undefined;
        }

        creature.updateMovementDirections();
    }

    /**
     * Updates the stored mouse position
     * @param position
     */
    updateMousePosition(position: Location) {
        this.mousePosition = position;
    }

    /**
     * Updates the facing of the passed creature
     * @param creature
     */
    updateCreatureFacing(creature: Creature, player?: Player) {
        const creatureLocation = creature.getLocation();

        if (creature instanceof Player) {
            creature.setFacing(new Vector(
                this.mousePosition.x - creatureLocation.x,
                this.mousePosition.y - creatureLocation.y
            ));
        } else {
            const playerVec = player.getLocationAsVector();
            const creatureVec = creature.getLocationAsVector();
            const diff = (playerVec.subtract(creatureVec)).normalize();
            const distance = MathService.getDistanceBetweenObjects(player, creature);

            if (distance <= creature.getLineOfSight()) {
                creature.setFacing(diff);
            }
        }
    }

    /**
     * Checks for collision of passed creature with other objects, if collides, stops movement
     * @param creature
     */
    private checkForCreatureCollision(creature: Creature, creatures: Creature[]) {
        if (this.detectCollisionForCreature(creature, creatures)) {
            this.reverseCreature(creature);
        }
    }

    /**
     * Detects if the passed creature collides with anything
     * @param creature
     */
    private detectCollisionForCreature(creature: Creature, creatures: Creature[]): boolean {
        this.mapService.setCollisionObjects(creatures);
        const candidates = this.mapService.getCollisionCandidates(creature);

        for (const candidate of candidates) {
            if (CollisionService.detectCollision(creature, candidate)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Moves a creature in all directions it is enabled to move
     * @param creature
     */
    private moveCreature(creature: Creature) {
        if (!(creature instanceof Player)) {
            creature.updateMovementDirections();
        }

        const directions = creature.getMovementDirections();

        if (directions[Direction.UP]) {
            this.moveUp(creature);
        }

        if (directions[Direction.DOWN]) {
            this.moveDown(creature);
        }

        if (directions[Direction.LEFT]) {
            this.moveLeft(creature);
        }

        if (directions[Direction.RIGHT]) {
            this.moveRight(creature);
        }
    }

    /**
     * Moves the passed creature up by it's speed value or the maximum distance it can move from the edge
     * @param creature
     */
    private moveUp(creature: Creature) {
        const halfSize = creature.getHalfSize();
        const distFromTopEdge = creature.getLocation().y - halfSize;

        if (creature.getLocation().y - halfSize - creature.getSpeed() >= 0) {
            creature.moveDirection(Direction.UP);
        } else if (distFromTopEdge > 0) {
            creature.moveDirection(Direction.UP, distFromTopEdge);
        }
    }

    /**
     * Moves the passed creature down by it's speed value or the maximum distance it can move from the edge
     * @param creature
     */
    private moveDown(creature: Creature) {
        const canvasHeight = this.canvasGlobals.canvasHeight;
        const creatureHalfSize = creature.getHalfSize();
        const creatureLocation = creature.getLocation();
        const distFromBottomEdge = canvasHeight - (creatureLocation.y + creatureHalfSize);

        if (creatureLocation.y + creatureHalfSize + creature.getSpeed() < canvasHeight) {
            creature.moveDirection(Direction.DOWN);
        } else if (distFromBottomEdge > 0) {
            creature.moveDirection(Direction.DOWN, distFromBottomEdge);
        }
    }

    /**
     * Moves the passed creature left by it's speed value or the maximum distance it can move from the edge
     * @param creature
     */
    private moveLeft(creature: Creature) {
        const halfSize = creature.getHalfSize();
        const distFromLeftEdge = creature.getLocation().x - halfSize;

        if (creature.getLocation().x - halfSize - creature.getSpeed() >= 0) {
            creature.moveDirection(Direction.LEFT);
        } else if (distFromLeftEdge > 0) {
            creature.moveDirection(Direction.LEFT, distFromLeftEdge);
        }
    }

    /**
     * Moves the passed creature right by it's speed value or the maximum distance it can move from the edge
     * @param creature
     */
    private moveRight(creature: Creature) {
        const canvasWidth = this.canvasGlobals.canvasWidth;
        const creatureSize = creature.getHalfSize();
        const creatureLocation = creature.getLocation();
        const distFromRightEdge = canvasWidth - (creatureLocation.x + creatureSize);

        if (creatureLocation.x + creatureSize + creature.getSpeed() < canvasWidth) {
            creature.moveDirection(Direction.RIGHT);
        } else if (distFromRightEdge > 0) {
            creature.moveDirection(Direction.RIGHT, distFromRightEdge);
        }
    }

    /**
     * Moves the passed creature in the opposite directions it is currently enabled to move, invalidating it's previous move turn
     * @param creature
     */
    private reverseCreature(creature: Creature) {
        const directions = creature.getMovementDirections();

        if (directions[Direction.UP]) {
            this.moveDown(creature);
        }

        if (directions[Direction.DOWN]) {
            this.moveUp(creature);
        }

        if (directions[Direction.LEFT]) {
            this.moveRight(creature);
        }

        if (directions[Direction.RIGHT]) {
            this.moveLeft(creature);
        }
    }

    /**
     * Enables the creature to move down
     * @param creature
     */
    private startDown(creature: Creature) {
        creature.startDirection(Direction.DOWN);
    }

    /**
     * Enables the creature to move left
     * @param creature
     */
    private startLeft(creature: Creature) {
        creature.startDirection(Direction.LEFT);
    }

    /**
     * Enables the creature to move right
    * @param creature
     */
    private startRight(creature: Creature) {
        creature.startDirection(Direction.RIGHT);
    }

    /**
     * Enables the creature to move up
     * @param creature
     */
    private startUp(creature: Creature) {
        creature.startDirection(Direction.UP);
    }

    /**
     * Disables the creature from moving down
     * @param creature
     */
    private stopDown(creature: Creature) {
        creature.stopDirection(Direction.DOWN);
    }

    /**
     * Disables the creature from moving left
     * @param creature
     */
    private stopLeft(creature: Creature) {
        creature.stopDirection(Direction.LEFT);
    }

    /**
     * Disables the creature from moving right
     * @param creature
     */
    private stopRight(creature: Creature) {
        creature.stopDirection(Direction.RIGHT);
    }

    /**
     * Disables the creature from moving up
     * @param creature
     */
    private stopUp(creature: Creature) {
        creature.stopDirection(Direction.UP);
    }
}
