import { TestBed, inject } from '@angular/core/testing';

import { CreatureService } from './creature.service';
import {Creature} from '../creature/creature';
import {Location} from '../../map/location';
import {Vector} from '../../../import/vector/vector';
import {CreatureFactory} from '../creature/factory/creature.factory';

describe('CreatureService', () => {
    let creatureFactory;
    let service;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [CreatureService]
        });
    });

    beforeEach(inject([CreatureService], (ss) => {
        creatureFactory = new CreatureFactory();
        service = ss;
    }));

    it('should be created', () => {
        expect(service).toBeTruthy();
        expect(service.getPlayer()).toBeTruthy();
    });

    it('should add creature to queue and get map-object', () => {
        const creature = creatureFactory.getGoblin(new Location(0, 0), new Vector(0, 1));
        let creatureList = service.getCreatures();

        expect(creatureList.length).toBe(2);

        service.addCreature(creature);
        creatureList = service.getCreatures();

        expect(creatureList.length).toBe(3);
    });

    it('should set player', () => {
        const player = new Creature(new Location(0, 0), new Vector(0, 1), 10, 20, .5, 20, []);

        service.setPlayer(player);

        expect(service.getPlayer()).toBe(player);
    });

    it('should remove creature', () => {
        const goblin = creatureFactory.getGoblin(
            new Location(10, 10),
            new Vector(0, 1)
        );

        service.addCreature(goblin);

        expect(service.getCreatures().length).toBe(3);

        service.removeCreature(goblin);

        expect(service.getCreatures().length).toBe(2);
    });
});
