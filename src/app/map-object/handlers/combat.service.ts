import { Injectable } from '@angular/core';
import {MapObject} from '../map-object';
import {MapService} from '../../map/map.service';
import {CollisionService} from '../service/collision.service';
import { CreatureService } from './creature.service';
import { Creature } from '../creature/creature';

@Injectable({
    providedIn: 'root'
})
export class CombatService {

    constructor(
        private creatureService: CreatureService,
        private mapService: MapService
    ) {}

    private dealDamageToObject(object: MapObject, amount: number) {
        object.takeDamage(amount);
        this.checkIfDestroyed(object);
    }

    private checkIfDestroyed(object: MapObject) {
        if (object.getCurrentHealth() <= 0) {
            if (object instanceof Creature) {
                this.creatureService.removeCreature(object);
            } else if (object instanceof MapObject) {
                this.mapService.removeMapObject(object);
            }
            object.onDestroy();
        }
    }

    detectHits() {
        const hitBoxes = this.mapService.getHitBoxes();

        for (const hitBox of hitBoxes) {
            const candidates = this.mapService.getCollisionCandidates(hitBox);

            for (const candidate of candidates) {
                if (hitBox.getOwner() !== candidate &&
                    CollisionService.detectHitBoxCollision(hitBox, candidate) &&
                    candidate.canTakeDamage()
                ) {
                    this.dealDamageToObject(candidate, hitBox.getDamage());
                }
            }
        }
    }

    initAttack(mapObject: MapObject) {
        this.mapService.addMapObject(mapObject.attack());
    }
}
