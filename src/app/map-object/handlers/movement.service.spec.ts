import { TestBed, inject } from '@angular/core/testing';

import { MovementService } from './movement.service';
import {Location} from '../../map/location';
import {MapService} from '../../map/map.service';
import {Vector} from '../../../import/vector/vector';
import {CreatureFactory} from '../creature/factory/creature.factory';
import {MapObjectFactory} from '../service/map-object-factory';
import { Direction } from '../../map/direction.enum';

describe('MovementService', () => {
    let canvas;
    let player;
    let creatureFactory;
    let service;

    beforeEach(() => {
        canvas = {height: 768, width: 1024};

        TestBed.configureTestingModule({
            providers: [
                MovementService,
                {
                    provide: MapService, useValue: {
                        canvas: canvas,
                        setCollisionObjects: function () {},
                        getCollisionCandidates: function () { return []; },
                        getCanvas: function () {return {height: 768, width: 1024}; }
                    }
                }
            ]
        });

        creatureFactory = new CreatureFactory();
        player = creatureFactory.getPlayer(new Location(10, 10), new Vector(0, 1));
    });

    beforeEach(inject([MovementService], (ss) => {
        service = ss;
    }));

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should move full speed correctly', () => {
        const creatures = [player];

        service.startUp(player);
        service.moveCreatures(creatures);
        expect(player.getLocation().y).toBe(10 - player.getSpeed());
        service.stopUp(player);
        service.moveCreatures(creatures);
        expect(player.getLocation().y).toBe(10 - player.getSpeed());

        service.startDown(player);
        service.moveCreatures(creatures);
        expect(player.getLocation().y).toBe(10);
        service.stopDown(player);
        service.moveCreatures(creatures);
        expect(player.getLocation().y).toBe(10);

        service.startLeft(player);
        service.moveCreatures(creatures);
        expect(player.getLocation().x).toBe(10 - player.getSpeed());
        service.stopLeft(player);
        service.moveCreatures(creatures);
        expect(player.getLocation().x).toBe(10 - player.getSpeed());

        service.startRight(player);
        service.moveCreatures(creatures);
        expect(player.getLocation().x).toBe(10);
        service.stopRight(player);
        service.moveCreatures(creatures);
        expect(player.getLocation().x).toBe(10);
    });

    it('should move as far as possible', () => {
        const xPos = canvas.width - 9;
        const yPos = canvas.height - 9;

        player = creatureFactory.getPlayer(new Location(xPos, yPos), new Vector(0, 1));
        const creatures = [player];

        service.startDown(player);
        service.moveCreatures(creatures);
        expect(player.getLocation().y).toBe(canvas.height - player.getHalfSize());
        service.stopDown(player);

        service.startRight(player);
        service.moveCreatures(creatures);
        expect(player.getLocation().x).toBe(canvas.width - player.getHalfSize());
        service.stopRight(player);

        player.location = new Location(8, 8);

        player.startDirection(Direction.UP);
        service.moveCreatures(creatures);
        expect(player.location.y).toBe(player.getHalfSize());
        player.stopDirection(Direction.DOWN);

        player.startDirection(Direction.LEFT);
        service.moveCreatures(creatures);
        expect(player.location.x).toBe(player.getHalfSize());
        player.stopDirection(Direction.LEFT);
    });

    it('should update mouse position', () => {
        const newMousePosition = new Location(10, 10);

        expect(service.mousePosition).toEqual(new Location(100, 100));

        service.updateMousePosition(newMousePosition);

        expect(service.mousePosition).toEqual(new Location(10, 10));
    });

    it('should detect collision to up', () => {
        player.location = new Location(30, 30);

        const collisionCandidatesSpy = spyOn(TestBed.get(MapService), 'getCollisionCandidates')
            .and.returnValue([MapObjectFactory.getWall(new Location(30, 20), new Vector(0, -1))]);
        const creatureMoveSpy = spyOn(player, 'moveDirection');

        player.startDirection(Direction.UP);
        service.moveCreatures([player]);
        player.stopDirection(Direction.UP);

        expect(collisionCandidatesSpy).toHaveBeenCalledTimes(1);
        expect(creatureMoveSpy).toHaveBeenCalledTimes(2);
        expect(creatureMoveSpy).toHaveBeenCalledWith(Direction.UP);
        expect(creatureMoveSpy).toHaveBeenCalledWith(Direction.DOWN);
    });

    it('should detect collision to down', () => {
        const collisionCandidatesSpy = spyOn(TestBed.get(MapService), 'getCollisionCandidates')
            .and.returnValue([MapObjectFactory.getWall(new Location(30, 20), new Vector(0, -1))]);
        const creatureMoveSpy = spyOn(player, 'moveDirection');

        player.startDirection(Direction.DOWN);
        service.moveCreatures([player]);
        player.stopDirection(Direction.DOWN);

        expect(collisionCandidatesSpy).toHaveBeenCalledTimes(1);
        expect(creatureMoveSpy).toHaveBeenCalledTimes(2);
        expect(creatureMoveSpy).toHaveBeenCalledWith(Direction.DOWN);
        expect(creatureMoveSpy).toHaveBeenCalledWith(Direction.UP);
    });

    it('should detect collision to left', () => {
        player.location = new Location(30, 30);

        const collisionCandidatesSpy = spyOn(TestBed.get(MapService), 'getCollisionCandidates')
            .and.returnValue([MapObjectFactory.getWall(new Location(30, 20), new Vector(0, -1))]);
        const creatureMoveSpy = spyOn(player, 'moveDirection');

        player.startDirection(Direction.LEFT);
        service.moveCreatures([player]);
        player.stopDirection(Direction.LEFT);

        expect(collisionCandidatesSpy).toHaveBeenCalledTimes(1);
        expect(creatureMoveSpy).toHaveBeenCalledTimes(2);
        expect(creatureMoveSpy).toHaveBeenCalledWith(Direction.LEFT);
        expect(creatureMoveSpy).toHaveBeenCalledWith(Direction.RIGHT);
    });

    it('should detect collision to right', () => {
        const collisionCandidatesSpy = spyOn(TestBed.get(MapService), 'getCollisionCandidates')
            .and.returnValue([MapObjectFactory.getWall(new Location(30, 20), new Vector(0, -1))]);
        const creatureMoveSpy = spyOn(player, 'moveDirection');

        player.startDirection(Direction.RIGHT);
        service.moveCreatures([player]);
        player.stopDirection(Direction.RIGHT);

        expect(collisionCandidatesSpy).toHaveBeenCalledTimes(1);
        expect(creatureMoveSpy).toHaveBeenCalledTimes(2);
        expect(creatureMoveSpy).toHaveBeenCalledWith(Direction.RIGHT);
        expect(creatureMoveSpy).toHaveBeenCalledWith(Direction.LEFT);
    });
});
