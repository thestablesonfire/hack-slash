import { Injectable } from '@angular/core';
import {Creature} from '../creature/creature';
import {CreatureFactory} from '../creature/factory/creature.factory';
import {Location} from '../../map/location';
import {Player} from '../creature/player/player';
import {Vector} from '../../../import/vector/vector';

@Injectable({
    providedIn: 'root'
})
export class CreatureService {
    private readonly creatureQueue: Creature[];
    private player: Player;

    /**
     * Holds all creatures that are instantiated during the execution of the application
     * @param creatureFactory
     */
    constructor(private creatureFactory: CreatureFactory) {
        this.creatureQueue = [];
        this.player = this.creatureFactory.getPlayer(
            new Location(100, 100),
            new Vector(0, 1).normalize()
        );

        this.addCreature(
            this.creatureFactory.getGoblin(
                new Location(550, 550),
                new Vector(-1, -1)
            )
        );
    }

    /**
     * Adds a creature to the creatureQueue
     * @param creature
     */
    addCreature(creature: Creature) {
        this.creatureQueue.push(creature);
    }

    /**
     * Returns an array with the player in position [0] and all creatures in the queue concatenated
     */
    getCreatures() {
        const creatures = [];

        creatures[0] = this.player;

        return creatures.concat(this.creatureQueue);
    }

    /**
     * Returns the player
     */
    getPlayer() {
        return this.player;
    }

    /**
     * Removes the passed creature from the creatureQueue
     * @param creature
     */
    removeCreature(creature: Creature) {
        const creatureIndex = this.creatureQueue.indexOf(creature);

        if (creatureIndex !== -1) {
            this.creatureQueue.splice(creatureIndex, 1);
        } else {
            console.log('error: tried to remove creature but creature not found');
        }
    }

    /**
     * Sets the player object
     * @param player
     */
    setPlayer(player: Player) {
        this.player = player;
    }
}
