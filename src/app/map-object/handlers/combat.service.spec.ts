import { TestBed, inject } from '@angular/core/testing';

import { CombatService } from './combat.service';
import {MapService} from '../../map/map.service';
import {CreatureFactory} from '../creature/factory/creature.factory';
import {Location} from '../../map/location';
import {Vector} from '../../../import/vector/vector';
import {HitBox} from '../creature/hit-box';
import {CollisionService} from '../service/collision.service';

describe('CombatService', () => {
    let player;
    let goblin;
    let service;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [CombatService]
        });
    });

    beforeEach(inject([CombatService], (ss) => {
        player = new CreatureFactory().getPlayer(
            new Location(20, 20),
            new Vector(0, 1)
        );
        goblin = new CreatureFactory().getGoblin(
            new Location(20, 20),
            new Vector(0, 1)
        );
        service = ss;
    }));

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should initiate an attack', () => {
        const addSpy = spyOn(TestBed.get(MapService), 'addMapObject');

        service.initAttack(goblin);

        expect(addSpy).toHaveBeenCalledTimes(1);
        expect(addSpy).toHaveBeenCalledWith(goblin.attack());
    });

    it('should detect hits', () => {
        const mapService = TestBed.get(MapService);
        const hitBoxesSpy = spyOn(mapService, 'getHitBoxes').and.returnValue([
            new HitBox(
                new Location(20, 20),
                new Vector(0, 1),
                20,
                player,
                4
            )
        ]);
        const candidatesSpy = spyOn(mapService, 'getCollisionCandidates').and.returnValue([
            goblin
        ]);
        const collisionSpy = spyOn(CollisionService, 'detectHitBoxCollision').and.returnValue(true);
        const removeObjSpy = spyOn(TestBed.get(MapService), 'removeMapObject');

        service.detectHits();

        expect(hitBoxesSpy).toHaveBeenCalledTimes(1);
        expect(candidatesSpy).toHaveBeenCalledTimes(1);
        expect(collisionSpy).toHaveBeenCalledTimes(1);
        expect(goblin.getCurrentHealth()).toEqual(1);
        expect(removeObjSpy).toHaveBeenCalledTimes(0);

        service.detectHits();

        expect(removeObjSpy).toHaveBeenCalledTimes(0);
    });

    it('should detect no hits from owner', () => {
        const mapService = TestBed.get(MapService);
        const hitBoxesSpy = spyOn(mapService, 'getHitBoxes').and.returnValue([
            new HitBox(
                new Location(20, 20),
                new Vector(0, 1),
                20,
                goblin,
                4
            )
        ]);
        const candidatesSpy = spyOn(mapService, 'getCollisionCandidates').and.returnValue([
            goblin
        ]);
        const collisionSpy = spyOn(CollisionService, 'detectHitBoxCollision').and.returnValue(true);
        const removeObjSpy = spyOn(TestBed.get(MapService), 'removeMapObject');

        service.detectHits();

        expect(hitBoxesSpy).toHaveBeenCalledTimes(1);
        expect(candidatesSpy).toHaveBeenCalledTimes(1);
        expect(collisionSpy).toHaveBeenCalledTimes(0);
        expect(goblin.getCurrentHealth()).toEqual(5);
        expect(removeObjSpy).toHaveBeenCalledTimes(0);
    });
});
