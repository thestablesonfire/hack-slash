import {Creature} from '../../creature';
import {Location} from '../../../../map/location';
import {Vector} from '../../../../../import/vector/vector';
import {Goblin} from './goblin';

let goblin;

describe('Creature', () => {
    beforeEach(function () {
        goblin = new Goblin(
            new Location(10, 10),
            new Vector(1, 1),
        );
    });

    it('should create the creature', () => {
        expect(goblin).toBeTruthy();
        expect(goblin.location).toEqual(new Location(10, 10));
        expect(goblin.facing).toEqual(new Vector(1, 1).normalize());
        expect(goblin.size).toEqual(10);
        expect(goblin.health).toEqual(5);
    });
});