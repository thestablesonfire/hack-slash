import {Creature} from '../../creature';
import {Vector} from '../../../../../import/vector/vector';
import {Location} from '../../../../map/location';
import {WeaponFactory} from '../../../item/weapon/factory/weapon-factory';

export class Goblin extends Creature {
    constructor(location: Location, facing: Vector) {
        super(location, facing, 10, 5, .5, 250, [WeaponFactory.getDagger()]);
    }
}
