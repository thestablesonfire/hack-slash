import { TestBed, inject } from '@angular/core/testing';

import { CreatureFactory } from './creature.factory';

describe('CreatureFactory', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreatureFactory]
    });
  });

  it('should be created', inject([CreatureFactory], (service: CreatureFactory) => {
    expect(service).toBeTruthy();
  }));
});
