import { Injectable } from '@angular/core';
import {Player} from '../player/player';
import {Vector} from '../../../../import/vector/vector';
import {Location} from '../../../map/location';
import {Goblin} from '../types/goblin/goblin';
import {WeaponFactory} from '../../item/weapon/factory/weapon-factory';

@Injectable({
    providedIn: 'root'
})
export class CreatureFactory {
    constructor() {}

    getPlayer(location: Location, facing: Vector): Player {
        return new Player(location, facing, [WeaponFactory.getshortSword()]);
    }

    getGoblin(location: Location, facing: Vector): Goblin {

        return new Goblin(location, facing);
    }
}
