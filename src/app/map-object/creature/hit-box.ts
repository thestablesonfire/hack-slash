import {BoundingBox} from './bounding-box';
import {MapObject} from '../map-object';
import {Vector} from '../../../import/vector/vector';

export class HitBox extends BoundingBox {
    /**
     * A representation of an area where and attack took plack
     * @param location
     * @param facing
     * @param size
     * @param owner
     * @param damage
     */
    constructor(
        protected location,
        protected facing: Vector,
        protected size,
        protected owner: MapObject,
        protected damage: number
    ) {
        super(location, facing, size);
        this.setOwner(owner);
    }

    /**
     * Returns the underlying bounding box of this hit box
     */
    getBoundingBox(): BoundingBox {
        return new BoundingBox(this.location, this.facing, this.size);
    }

    /**
     * Returns the damage this hitbox would cause
     */
    getDamage() {
        return this.damage;
    }
}
