import {Location} from '../../map/location';
import {MapObject} from '../map-object';
import {Vector} from '../../../import/vector/vector';
import {MathService} from '../../services/math/math.service';
import { MapEntity } from '../map-entity';

export class BoundingBox extends MapEntity {
    protected damageable: boolean;
    protected color: string;
    protected owner: MapObject;
    protected readonly renderCanvas: HTMLCanvasElement;
    protected readonly renderCtx: CanvasRenderingContext2D;

    /**
     * The base class of anything that is on the map
     * @param location
     * @param facing
     * @param size
     */
    constructor(protected location: Location, protected facing: Vector, protected size) {
        super(location, facing, size);
        this.damageable = false;
        this.color = '#F00';
        this.renderCanvas = document.createElement('canvas');
        this.renderCtx = this.renderCanvas.getContext('2d');
    }

    /**
     * Clears the canvas used to draw this object
     */
    private clearRenderCanvas() {
        this.renderCtx.clearRect(0, 0, this.renderCanvas.width, this.renderCanvas.height);
    }

    /**
     * Draws the bounding box square
     * @param points
     */
    protected drawShape(points: Location[]) {
        this.renderCtx.beginPath();
        this.setStroke(this.renderCtx, 1, '#F00');
        this.renderCtx.moveTo(points[0].x, points[0].y);
        this.renderCtx.lineTo(points[1].x, points[1].y);
        this.renderCtx.moveTo(points[1].x, points[1].y);
        this.renderCtx.lineTo(points[2].x, points[2].y);
        this.renderCtx.lineTo(points[3].x, points[3].y);
        this.renderCtx.lineTo(points[0].x, points[0].y);
        this.renderCtx.stroke();
        this.renderCtx.closePath();
    }

    /**
     * Returns the 4 points of the bounding box
     * @returns Location[]
     */
    private getRenderPoints(): Location[] {
        const widestDimension = this.getWidestDimension();
        const midpoint = new Location(widestDimension / 2, widestDimension / 2);
        const halfSize = this.getHalfSize();
        const facingAngle = this.facing.horizontalAngle();
        const rotatePoint = MathService.rotatePoint;


        return [
            rotatePoint(midpoint, new Location(midpoint.x + halfSize, midpoint.y - halfSize), facingAngle),
            rotatePoint(midpoint, new Location(midpoint.x + halfSize, midpoint.y + halfSize), facingAngle),
            rotatePoint(midpoint, new Location(midpoint.x - halfSize, midpoint.y + halfSize), facingAngle),
            rotatePoint(midpoint, new Location(midpoint.x - halfSize, midpoint.y - halfSize), facingAngle)
        ];
    }

    /**
     * Draws the shape to the pre-render canvas and returns that canvas
     * @returns HTMLCanvasElement
     */
    protected preRender(): HTMLCanvasElement {
        this.clearRenderCanvas();

        // DEBUG show bounding box
        // this.renderCtx.strokeRect(0, 0, this.getWidestDimension(), this.getWidestDimension());

        this.drawShape(this.getRenderPoints());

        return this.renderCanvas;
    }

    /**
     * Sets the stroke style
     */
    protected setStroke(ctx, width: number, color: string) {
        ctx.lineWidth = width;
        ctx.strokeStyle = color;
    }

    /**
     * Returns whether the object can take damage
     */
    canTakeDamage() {
        return this.damageable;
    }

    /**
     * Draws the bounding box
     */
    draw(ctx: CanvasRenderingContext2D): void {
        const halfSize = this.getWidestDimension() / 2;

        ctx.beginPath();
        ctx.drawImage(this.preRender(), this.location.x - halfSize, this.location.y - halfSize);
        ctx.closePath();
    }
}
