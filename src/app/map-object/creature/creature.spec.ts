import {Creature} from './creature';
import {Location} from '../../map/location';
import {noop} from 'rxjs';
import {Vector} from '../../../import/vector/vector';
import { Direction } from '../../map/direction.enum';

let creature;

describe('Creature', () => {
    beforeEach(function () {
        creature = new Creature(
            new Location(10, 10),
            new Vector(0, 1),
            10,
            20,
            2,
            5,
            [],
        );
    });

    it('should create the creature', () => {
        expect(creature).toBeTruthy();
    });

    it('should draw itself', () => {
        const ctx = {rect: noop, fill: noop, stroke: noop, beginPath: noop, moveTo: noop, lineTo: noop, drawImage: noop, closePath: noop};
        const canvasBeginPathSpy = spyOn(ctx, 'beginPath');
        const canvasDrawImageSpy = spyOn(ctx, 'drawImage');
        const canvasClosePathSpy = spyOn(ctx, 'closePath');

        creature.draw(ctx);

        expect(canvasBeginPathSpy).toHaveBeenCalledTimes(1);
        expect(canvasDrawImageSpy).toHaveBeenCalledTimes(1);
        expect(canvasClosePathSpy).toHaveBeenCalledTimes(1);
    });

    it('should get all values correctly', () => {
        expect(creature.getLocation()).toEqual(new Location(10, 10));
        expect(creature.getMovementDirections()).toEqual({Up: false, Down: false, Left: false, Right: false});
        expect(creature.getHalfSize()).toEqual(5);
        expect(creature.getSpeed()).toEqual(2);
    });

    it('should move up correctly', () => {
        let movementDirections;
        const moveSpy = spyOn(creature.getLocation(), 'moveUp');
        creature.moveDirection(Direction.UP);
        expect(moveSpy).toHaveBeenCalledTimes(1);

        creature.startDirection(Direction.UP);
        movementDirections = creature.getMovementDirections();
        creature.moveDirection(Direction.UP);

        expect(movementDirections.Up).toBe(true);
        expect(moveSpy).toHaveBeenCalledTimes(2);
        expect(moveSpy).toHaveBeenCalledWith(creature.getSpeed());

        creature.stopDirection(Direction.UP);
        movementDirections = creature.getMovementDirections();
        expect(movementDirections.Up).toBe(false);
        creature.moveDirection(Direction.UP);
        expect(moveSpy).toHaveBeenCalledTimes(3);
    });

    it('should move down correctly', () => {
        let movementDirections;
        const moveSpy = spyOn(creature.getLocation(), 'moveDown');
        creature.moveDirection(Direction.DOWN);
        expect(moveSpy).toHaveBeenCalledTimes(1);

        creature.startDirection(Direction.DOWN);
        movementDirections = creature.getMovementDirections();
        creature.moveDirection(Direction.DOWN);

        expect(movementDirections.Down).toBe(true);
        expect(moveSpy).toHaveBeenCalledTimes(2);

        creature.stopDirection(Direction.DOWN);
        movementDirections = creature.getMovementDirections();
        expect(movementDirections.Down).toBe(false);
        creature.moveDirection(Direction.DOWN);
        expect(moveSpy).toHaveBeenCalledTimes(3);
    });

    it('should move left correctly', () => {
        let movementDirections;
        const moveSpy = spyOn(creature.getLocation(), 'moveLeft');
        creature.moveDirection(Direction.LEFT);
        expect(moveSpy).toHaveBeenCalledTimes(1);

        creature.startDirection(Direction.LEFT);
        movementDirections = creature.getMovementDirections();
        creature.moveDirection(Direction.LEFT);

        expect(movementDirections.Left).toBe(true);
        expect(moveSpy).toHaveBeenCalledTimes(2);

        creature.stopDirection(Direction.LEFT);
        movementDirections = creature.getMovementDirections();
        expect(movementDirections.Left).toBe(false);
        creature.moveDirection(Direction.LEFT);
        expect(moveSpy).toHaveBeenCalledTimes(3);
    });

    it('should move right correctly', () => {
        let movementDirections;
        const moveSpy = spyOn(creature.getLocation(), 'moveRight');
        creature.moveDirection(Direction.RIGHT);
        expect(moveSpy).toHaveBeenCalledTimes(1);

        creature.startDirection(Direction.RIGHT);
        movementDirections = creature.getMovementDirections();
        creature.moveDirection(Direction.RIGHT);

        expect(movementDirections.Right).toBe(true);
        expect(moveSpy).toHaveBeenCalledTimes(2);

        creature.stopDirection(Direction.RIGHT);
        movementDirections = creature.getMovementDirections();
        expect(movementDirections.Right).toBe(false);
        creature.moveDirection(Direction.RIGHT);
        expect(moveSpy).toHaveBeenCalledTimes(3);
    });

    it('should move the correct distance', () => {
        const moveDist = 1;
        const moveSpyUp = spyOn(creature.getLocation(), 'moveUp');
        const moveSpyDown = spyOn(creature.getLocation(), 'moveDown');
        const moveSpyLeft = spyOn(creature.getLocation(), 'moveLeft');
        const moveSpyRight = spyOn(creature.getLocation(), 'moveRight');

        creature.startDirection(Direction.UP);
        creature.startDirection(Direction.DOWN);
        creature.startDirection(Direction.LEFT);
        creature.startDirection(Direction.RIGHT);
        creature.moveDirection(Direction.UP, moveDist);
        creature.moveDirection(Direction.DOWN, moveDist);
        creature.moveDirection(Direction.LEFT, moveDist);
        creature.moveDirection(Direction.RIGHT, moveDist);

        expect(moveSpyUp).toHaveBeenCalledTimes(1);
        expect(moveSpyUp).toHaveBeenCalledWith(moveDist);
        expect(moveSpyDown).toHaveBeenCalledTimes(1);
        expect(moveSpyDown).toHaveBeenCalledWith(moveDist);
        expect(moveSpyLeft).toHaveBeenCalledTimes(1);
        expect(moveSpyLeft).toHaveBeenCalledWith(moveDist);
        expect(moveSpyRight).toHaveBeenCalledTimes(1);
        expect(moveSpyRight).toHaveBeenCalledWith(moveDist);
    });
});
