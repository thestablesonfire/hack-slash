import { EventEmitter, Injectable } from '@angular/core';
import { CreatureNest } from './creature-nest';
import { AddCreatureEvent } from 'src/app/events/add-creature-event';

@Injectable({
    providedIn: 'root'
})
export class NestService {
    creatureCreated: EventEmitter<AddCreatureEvent>;
    private nests: CreatureNest[];

    constructor() {
        this.nests = [];
        this.creatureCreated = new EventEmitter();
    }

    /**
     * Adds a nest to the list
     * @param newNest
     */
    addNest(newNest: CreatureNest) {
        newNest.setEmitter(this.creatureCreated);
        this.nests.push(newNest);
    }
}
