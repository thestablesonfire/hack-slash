import { TestBed } from '@angular/core/testing';

import { NestService } from './nest.service';

describe('NestServiceService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: NestService = TestBed.get(NestService);
        expect(service).toBeTruthy();
    });
});
