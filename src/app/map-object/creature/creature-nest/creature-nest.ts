import { Creature } from '../creature';
import { MapObject } from '../../map-object';
import { Vector } from 'src/import/vector/vector';
import { Location } from 'src/app/map/location';
import { MathService } from 'src/app/services/math/math.service';
import { EventEmitter } from '@angular/core';
import { AddCreatureEvent } from 'src/app/events/add-creature-event';

export class CreatureNest extends MapObject {
    interval: number;
    newCreatureEmitter: EventEmitter<AddCreatureEvent>;

    /**
     * A Map Object that generates creatures
     * @param location
     * @param facing
     * @param size
     * @param health
     * @param creature
     * @param range
     * @param time
     */
    constructor(
        location: Location,
        facing: Vector,
        size: number,
        health: number,
        private creature: Creature,
        private maxCreatures: number,
        private range,
        private time: number
    ) {
        super(location, facing, size, health, true, false, []);
        this.interval = window.setInterval(() => {
            this.makeCreature();
        }, this.time);
    }

    /**
     * Returns the maximum # of creatures this nest can have created at once
     */
    getMaxCreatures() {
        return this.maxCreatures;
    }

    onDestroy() {
        clearInterval(this.interval);
    }

    setEmitter(emitter: EventEmitter<AddCreatureEvent>) {
        this.newCreatureEmitter = emitter;
    }

    private getPointInRange() {
        let number = MathService.getRandomNumberBetween(this.getWidestDimension(), this.range);

        return Math.random() > .5 ? number *= -1 : number;
    }

    private makeCreature() {
        const newRandomLocation = new Location(
            this.location.x + this.getPointInRange(),
            this.location.y + this.getPointInRange()
        );

        const creature = this.creature.clone();
        creature.setOwner(this);
        creature.setLocation(newRandomLocation);
        this.newCreatureEmitter.emit(new AddCreatureEvent(creature));
    }
}
