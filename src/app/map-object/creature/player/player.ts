import {Creature} from '../creature';
import {Location} from '../../../map/location';
import {Vector} from '../../../../import/vector/vector';
import {Weapon} from '../../item/weapon/weapon';

export class Player extends Creature {
    constructor(location: Location, facing: Vector, protected weapons: Weapon[]) {
        super(location, facing, 15, 20, 2, 0, weapons);
    }
}
