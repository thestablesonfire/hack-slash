import {Location} from '../../map/location';
import {Vector} from '../../../import/vector/vector';
import {MapObject} from '../map-object';
import {BoundingBox} from './bounding-box';
import {Weapon} from '../item/weapon/weapon';
import { Direction } from '../../map/direction.enum';
import { PathfindingNode } from '../../services/pathfinder/pathfinding-node';


export class Creature extends MapObject {
    private isMoving: {};
    private path: PathfindingNode[];

    /**
     * A moveable, animate creature
     * @param location
     * @param facing
     * @param size
     * @param health
     * @param speed
     * @param weapons
     */
    constructor(
        location: Location,
        facing: Vector,
        size: number,
        health: number,
        private speed: number,
        private lineOfSight: number,
        weapons: Weapon[],
    ) {
        super(location, facing, size, health, true, false, weapons);

        this.isMoving = {
            Up: false,
            Down: false,
            Left: false,
            Right: false
        };

        this.facing = this.facing.normalize();
        this.path = null;
    }

    /**
     * Clears the path
     */
    clearPath() {
        this.path = null;
    }

    clone(): Creature {
        return new Creature(
            this.location,
            this.facing,
            this.size,
            this.health,
            this.speed,
            this.lineOfSight,
            this.weapons,
        );
    }

    draw(ctx: CanvasRenderingContext2D) {
        const halfSize = this.getWidestDimension() / 2;

        ctx.beginPath();
        ctx.drawImage(this.preRender(), this.location.x - halfSize, this.location.y - halfSize);
        ctx.closePath();

        /*
        if (this.path && this.path.length) {
            ctx.beginPath();
            ctx.moveTo(this.location.x, this.location.y);
            for (let i = 0; i < this.path.length; i++) {
                ctx.lineTo(this.path[i].location.x, this.path[i].location.y);
            }
            ctx.stroke();
            ctx.closePath();
        }
        */
    }

    /**
     * Returns the underlying bounding box for this creature
     */
    getBoundingBox() {
        const halfWidest = this.getWidestDimension() / 2;

        return new BoundingBox(
            new Location(this.location.x - halfWidest, this.location.y - halfWidest),
            this.facing,
            this.getWidestDimension()
        );
    }

    /**
     * Returns the line of sight
     */
    getLineOfSight(): number {
        return this.lineOfSight;
    }

    /**
     * Returns the directions the creature is currently moving
     */
    getMovementDirections() {
        return this.isMoving;
    }

    /**
     * Returns the speed of the creature
     */
    getSpeed() {
        return this.speed;
    }

    /**
     * Moves the creature in the passed direction, if dist is passed then only moves it that far
     * @param direction
     * @param dist
     */
    moveDirection(direction: Direction, dist?: number) {
        switch (direction) {
            case Direction.DOWN:
                this.move('Down', dist);
            break;
            case Direction.LEFT:
                this.move('Left', dist);
            break;
            case Direction.RIGHT:
                this.move('Right', dist);
            break;
            case Direction.UP:
                this.move('Up', dist);
            break;
        }
    }

    /**
     * Updates the facing of the creature
     * @param facing
     */
    setFacing(facing: Vector) {
        this.facing = facing;
    }

    /**
     * Sets the creatures path toward the player
     * @param path
     */
    setPath(path: PathfindingNode[]) {
        this.path = path;
    }

    /**
     * Starts the creature moving in the passed direction
     * @param direction
     */
    startDirection(direction: Direction) {
        switch (direction) {
            case Direction.DOWN:
                this.startMoving('Down');
            break;
            case Direction.LEFT:
                this.startMoving('Left');
            break;
            case Direction.RIGHT:
                this.startMoving('Right');
            break;
            case Direction.UP:
                this.startMoving('Up');
            break;
        }
    }

    /**
     * Stops the creature moving in the passed direction
     * @param direction
     */
    stopDirection(direction: Direction) {
        switch (direction) {
            case Direction.DOWN:
                this.stopMoving('Down');
            break;
            case Direction.LEFT:
                this.stopMoving('Left');
            break;
            case Direction.RIGHT:
                this.stopMoving('Right');
            break;
            case Direction.UP:
                this.stopMoving('Up');
            break;
        }
    }

    /**
     * Update's the creature's movement directions based on their path
     */
    updateMovementDirections() {
        let pathLocation;
        this.clearMovementDirections();

        if (this.path && this.path.length) {
            pathLocation = this.path[0].location;

            if (
                this.location.x + this.speed >= pathLocation.x &&
                this.location.x - this.speed <= pathLocation.x &&
                this.location.y + this.speed >= pathLocation.y &&
                this.location.y - this.speed <= pathLocation.y
            ) {
                this.path.shift();
            }

            if (location) {
                // TODO: Figure out why the man keeps moving back and forth
                const directions = (new Vector(pathLocation.x, pathLocation.y)
                    .subtract(new Vector(this.location.x, this.location.y))).normalize();

                if (directions.x > 0) {
                    this.startMoving(Direction.RIGHT);
                } else if (directions.x < 0) {
                    this.startMoving(Direction.LEFT);
                }

                if (directions.y > 0) {
                    this.startMoving(Direction.DOWN);
                } else if (directions.y < 0) {
                    this.startMoving(Direction.UP);
                }

            }
        }
    }

    /**
     * Draws the creature to the pre-render canvas
     * @param points
     */
    protected drawShape(points: Location[]) {
        this.renderCtx.beginPath();
        this.setStroke(this.renderCtx, 1, '#F00');
        this.renderCtx.moveTo(points[0].x, points[0].y);
        this.renderCtx.lineTo(points[1].x, points[1].y);
        this.renderCtx.stroke();
        this.renderCtx.closePath();

        this.renderCtx.beginPath();
        this.setStroke(this.renderCtx, 1, '#000');
        this.renderCtx.moveTo(points[1].x, points[1].y);
        this.renderCtx.lineTo(points[2].x, points[2].y);
        this.renderCtx.lineTo(points[3].x, points[3].y);
        this.renderCtx.lineTo(points[0].x, points[0].y);
        this.renderCtx.stroke();
        this.renderCtx.closePath();
    }

    /**
     * Updates the creature's locaton
     * @param direction
     * @param dist
     */
    private move(direction: string, dist?: number) {
        this.location['move' + direction](dist ? dist : this.speed);
    }

    /**
     * Starts the creature moving in the passed direction
     * @param direction
     */
    private startMoving(direction) {
        this.isMoving[direction] = true;
    }

    /**
     * Stops the creature moving in the passed direction
     */
    private stopMoving(direction: string) {
        this.isMoving[direction] = false;
    }

    /**
     * Sets all movement directions to false
     */
    private clearMovementDirections() {
        this.isMoving[Direction.DOWN] = false;
        this.isMoving[Direction.LEFT] = false;
        this.isMoving[Direction.RIGHT] = false;
        this.isMoving[Direction.UP] = false;
    }
}
