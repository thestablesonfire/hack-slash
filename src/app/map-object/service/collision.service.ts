import { Injectable } from '@angular/core';
import {MapObject} from '../map-object';
import {HitBox} from '../creature/hit-box';
import {Location} from '../../map/location';
import { MapEntity } from '../map-entity';

@Injectable({
    providedIn: 'root'
})
export class CollisionService {

    // Rectangle-rectangle collision
    static detectCollision(creature: MapEntity, candidate: MapEntity): boolean {
        let result = false;

        if (creature !== candidate) {
            const candidateLocation = candidate.getLocation();
            const candidateSize = candidate.getHalfSize();
            const creatureLocation = creature.getLocation();
            const creatureSize = creature.getHalfSize();

            result = this.doesCollide(creatureLocation, creatureSize, candidateLocation, candidateSize);
        }

        return result;
    }

    static detectHitBoxCollision(hitBox: HitBox, candidate: MapObject) {
        let result = false;

        if (hitBox.getOwner() !== candidate) {
            const candidateBox = candidate.getBoundingBox();
            const candidateLocation = candidateBox.getLocation();
            const candidateSize = candidateBox.getSize();
            const creatureBox = hitBox.getBoundingBox();
            const creatureLocation = creatureBox.getLocation();
            const creatureSize = creatureBox.getSize();

            result = this.doesCollide(creatureLocation, creatureSize, candidateLocation, candidateSize);
        }

        return result;
    }

    /**
     * Returns true if the creature is within the bounds of the candidate
     * @param creatureLocation
     * @param creatureHalfSize
     * @param candidateLocation
     * @param candidateHalfSize
     */
    private static doesCollide(
        creatureLocation: Location,
        creatureHalfSize: number,
        candidateLocation: Location,
        candidateHalfSize: number
    ) {
        return creatureLocation.x - creatureHalfSize <= candidateLocation.x + candidateHalfSize &&
            creatureLocation.x + creatureHalfSize >= candidateLocation.x - candidateHalfSize &&
            creatureLocation.y - creatureHalfSize <= candidateLocation.y + candidateHalfSize &&
            creatureLocation.y + creatureHalfSize >= candidateLocation.y - candidateHalfSize;
    }
}
