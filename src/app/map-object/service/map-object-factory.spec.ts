import {MapObjectFactory} from './map-object-factory';
import {Vector} from '../../../import/vector/vector';
import {Location} from '../../map/location';

describe('MapObjectFactory', () => {
    beforeEach(function () {});

    it('should create the object', () => {
        expect(MapObjectFactory).toBeTruthy();
    });

    it('should create return a Wall', () => {
        const wall = MapObjectFactory.getWall(
            new Location(20, 20),
            new Vector(0, 1)
        );

        expect(wall.getIsPassable()).toEqual(false);
        expect(wall.canTakeDamage()).toEqual(false);
        expect(wall.getSize()).toEqual(50);
    });

    it('should create return a Tile', () => {
        const wall = MapObjectFactory.getTile(
            new Location(20, 20),
            new Vector(0, 1)
        );

        expect(wall.getIsPassable()).toEqual(true);
        expect(wall.canTakeDamage()).toEqual(false);
        expect(wall.getSize()).toEqual(10);
    });
});
