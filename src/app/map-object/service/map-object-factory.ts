import {MapObject} from '../map-object';
import {Location} from '../../map/location';
import {Vector} from '../../../import/vector/vector';
import { CreatureNest } from '../creature/creature-nest/creature-nest';
import { Creature } from '../creature/creature';

export class MapObjectFactory {
    static getNest(location: Location, facing: Vector, creature: Creature) {
        return new CreatureNest(location, facing, 30, 30, creature, 10, 100, 5000);
    }

    static getTile(location: Location, facing: Vector) {
        return new MapObject(location, facing, 10, 1, false, true, []);
    }

    static getWall(location: Location, facing: Vector): MapObject {
        return new MapObject(location, facing, 50, 1, false, false, []);
    }
}
