import { TestBed, inject } from '@angular/core/testing';

import { CollisionService } from './collision.service';
import {EventQueueService} from '../../events/event-queue/event-queue.service';
import {HitBox} from '../creature/hit-box';
import {Vector} from '../../../import/vector/vector';
import {Location} from '../../map/location';
import {CreatureFactory} from '../creature/factory/creature.factory';
import {MapObject} from '../map-object';

describe('CollisionService', () => {
    let goblin;
    let player;
    let service;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [CollisionService]
        });
    });

    beforeEach(inject([EventQueueService], (ss) => {
        player = new CreatureFactory().getPlayer(new Location(10, 10), new Vector(0, 1));
        goblin = new CreatureFactory().getGoblin(new Location(20, 20), new Vector(0, -1));
        service = ss;
    }));

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should detect a HitBox collision', () => {
        const hitBox = new HitBox(
            new Location(20, 20),
            new Vector(0, 1),
            15,
            player,
            5
        );

        expect(CollisionService.detectHitBoxCollision(hitBox, goblin)).toBe(true);
        expect(CollisionService.detectHitBoxCollision(hitBox, player)).toBe(false);
    });

    it('should detect a MapObject collision', () => {
        const mapObject = new MapObject(
            new Location(20, 20),
            new Vector(0, 1),
            15,
            5,
            true,
            false,
            []
        );

        expect(CollisionService.detectCollision(mapObject, goblin)).toBe(true);
        expect(CollisionService.detectCollision(mapObject, mapObject)).toBe(false);
    });
});
