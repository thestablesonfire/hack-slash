import {MeleeWeapon} from '../melee-weapon';

export class WeaponFactory {
    static getDagger() {
        return new MeleeWeapon('Dagger', 1, 3);
    }

    static getshortSword() {
        return new MeleeWeapon('Short Sword', 3, 10);
    }
}
