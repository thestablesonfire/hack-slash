import {CreatureFactory} from '../../creature/factory/creature.factory';
import {Vector} from '../../../../import/vector/vector';
import {Location} from '../../../map/location';
import {MeleeWeapon} from './melee-weapon';

let player;
let weapon;

describe('AttackEvent', () => {
    beforeEach(function () {
        player = new CreatureFactory().getPlayer(
            new Location(20, 20),
            new Vector(0, 1)
        );
        weapon = new MeleeWeapon('dagger', 2, 5);
    });

    it('should create the object', () => {
        expect(weapon).toBeTruthy();
    });
});
