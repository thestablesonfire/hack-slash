import {HitBox} from '../../creature/hit-box';
import {Location} from '../../../map/location';
import {MapObject} from '../../map-object';

export class Weapon {
    constructor(
        private name: string,
        private damage: number,
        private size: number
    ) {}

    private getHitboxLocation(location, owner) {
        const facing = owner.getFacing().normalize().clone();

        facing.multiplyScalar(owner.size + owner.size / 4);

        return new Location(facing.x + owner.location.x, facing.y + owner.location.y);
    }

    attack(location: Location, owner: MapObject): HitBox {
        const facing = owner.getFacing();

        return new HitBox(this.getHitboxLocation(location, owner), facing, this.size, owner, this.damage);
    }
}
