import {Weapon} from './weapon';

export class RangedWeapon extends Weapon {
    constructor(name: string, damage: number, size: number) {
        super(name, damage, size);
    }
}
