import {Weapon} from './weapon';

export class MeleeWeapon extends Weapon {
    constructor(name: string, damage: number, size: number) {
        super(name, damage, size);
    }
}
