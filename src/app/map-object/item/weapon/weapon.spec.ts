import {Weapon} from './weapon';
import {CreatureFactory} from '../../creature/factory/creature.factory';
import {Vector} from '../../../../import/vector/vector';
import {Location} from '../../../map/location';
import {HitBox} from '../../creature/hit-box';

let player;
let weapon;

describe('AttackEvent', () => {
    beforeEach(function () {
        player = new CreatureFactory().getPlayer(
            new Location(20, 20),
            new Vector(0, 1)
        );
        weapon = new Weapon('dagger', 2, 5);
    });

    it('should create the object', () => {
        expect(weapon).toBeTruthy();
    });

    it('should return the correct HitBox', () => {
        const hitBox = weapon.attack(player.location, player);

        expect(hitBox).toEqual(new HitBox(new Location(20, 38.75), new Vector(0, 1), 5, player, 2));
    });
});
