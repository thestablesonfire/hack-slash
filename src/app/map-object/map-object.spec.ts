import {MapObject} from './map-object';
import {Location} from '../map/location';
import {MapObjectFactory} from './service/map-object-factory';
import {Vector} from '../../import/vector/vector';

let object;

describe('Map Object', () => {
    beforeEach(function () {
        object = MapObjectFactory.getWall(new Location(50, 50), new Vector(0, -1));
    });

    it('should create the object', () => {
        expect(object).toBeTruthy();
    });

    it('should return if the object is passable or not', () => {
        expect(object.getIsPassable()).toBe(false);

        const newObject = MapObjectFactory.getTile(new Location(50, 50), new Vector(0, -1));

        expect(newObject.getIsPassable()).toBe(true);
    });
});
