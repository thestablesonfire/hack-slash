import {BoundingBox} from './creature/bounding-box';
import {Location} from '../map/location';
import {Weapon} from './item/weapon/weapon';
import {HitBox} from './creature/hit-box';
import {Vector} from '../../import/vector/vector';

export class MapObject extends BoundingBox {
    selectedWeapon: Weapon;
    protected currentHealth: number;

    /**
     * An object that will be seen on the map, usually inanimate
     * @param location
     * @param facing
     * @param size
     * @param health
     * @param damageable
     * @param isPassable
     * @param weapons
     */
    constructor(
        protected location,
        protected facing: Vector,
        protected size,
        protected health: number,
        protected damageable: boolean,
        private isPassable,
        protected weapons: Weapon[]
    ) {
        super(location, facing, size);
        this.color = '#666';
        this.currentHealth = health;
        this.selectedWeapon = this.weapons[0];
    }

    /**
     * Returns a HitBox representing an attack from this object
     */
    attack(): HitBox {
        return this.selectedWeapon.attack(this.location, this);
    }

    /**
     * Returns the underlying HitBox
     */
    getBoundingBox(): BoundingBox {
        const halfSize = this.getHalfSize();

        return new BoundingBox(
            new Location(this.location.x - halfSize, this.location.y - halfSize),
            this.facing,
            this.size
        );
    }

    /**
     * Returns the current health of the map object
     */
    getCurrentHealth() {
        return this.currentHealth;
    }

    /**
     * Returns the facing of the map object
     */
    getFacing() {
        return this.facing;
    }

    /**
     * Returns whether or not this object is passable
     */
    getIsPassable() {
        return this.isPassable;
    }

    /**
     * Returns this location as a vector
     */
    getLocationAsVector() {
        return new Vector(this.location.x, this.location.y);
    }

    onDestroy() {}

    /**
     * Sets the location for the character
     * @param location
     */
    setLocation (location: Location) {
        this.location = location;
    }

    /**
     * Returns wether or not this map object can take damage
     * @param damage
     */
    takeDamage(damage: number) {
        this.currentHealth -= damage;
    }

    /**
     * Draws the map object to the render canvas
     * @param points
     */
    protected drawShape(points: Location[]) {
        this.renderCtx.beginPath();
        this.setStroke(this.renderCtx, 1, '#000');
        this.renderCtx.moveTo(points[0].x, points[0].y);
        this.renderCtx.lineTo(points[1].x, points[1].y);
        this.renderCtx.moveTo(points[1].x, points[1].y);
        this.renderCtx.lineTo(points[2].x, points[2].y);
        this.renderCtx.lineTo(points[3].x, points[3].y);
        this.renderCtx.lineTo(points[0].x, points[0].y);
        this.renderCtx.stroke();
        this.renderCtx.closePath();
    }
}
