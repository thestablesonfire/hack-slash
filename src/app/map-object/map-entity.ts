import { MapObject } from './map-object';
import { Location } from '../map/location';
import { Vector } from 'src/import/vector/vector';

export class MapEntity {
    protected owner: MapObject;

    constructor(
        protected location: Location,
        protected facing: Vector,
        protected size: number
    ) {}

    /**
     * Returns the half-size of the bounding box
     */
    getHalfSize(): number {
        return this.size / 2;
    }

    /**
     * Returns the location of the bounding box
     */
    getLocation(): Location {
        return this.location;
    }

    /**
     * Returns the owner of the bounding box
     */
    getOwner() {
        return this.owner;
    }

    /**
     * Returns the full size of the bounding box
     */
    getSize() {
        return this.size;
    }

    /**
     * Returns the widest dimension of the bouding box
     */
    getWidestDimension() {
        return Math.sqrt( (this.size * this.size) * 2 );
    }

    /**
     * Sets the owner of the bounding box
     * @param owner
     */
    setOwner(owner: MapObject) {
        this.owner = owner;
    }
}
