import { TestBed, inject } from '@angular/core/testing';

import { CanvasGlobalsService } from './canvas-globals.service';

describe('CanvasGlobalsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanvasGlobalsService]
    });
  });

  it('should be created', inject([CanvasGlobalsService], (service: CanvasGlobalsService) => {
    expect(service).toBeTruthy();
  }));
});
