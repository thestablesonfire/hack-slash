import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DebugService {
    debugEnabled: boolean;

    constructor() {
        this.debugEnabled = false;
    }

    printMsg(message: string) {
        if (this.debugEnabled) {
            console.log(message);
        }
    }
}
