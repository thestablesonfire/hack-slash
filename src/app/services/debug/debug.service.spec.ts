import { TestBed, inject } from '@angular/core/testing';

import { DebugService } from './debug.service';

describe('DebugService', () => {
    let service;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [DebugService]
        });
    });

    beforeEach(inject([DebugService], (ss: DebugService) => {
        service = ss;
    }));

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should print a msg', () => {
        const debugSpy = spyOn(console, 'log');
        const testString = 'test';

        service.debugEnabled = true;
        service.printMsg(testString);

        expect(debugSpy).toHaveBeenCalledWith(testString);
    });

    it('should not print the message', () => {
        const noDebugSpy = spyOn(console, 'log');
        const testString = 'test';

        service.debugEnabled = false;
        service.printMsg(testString);

        expect(noDebugSpy).toHaveBeenCalledTimes(0);
    });
});
