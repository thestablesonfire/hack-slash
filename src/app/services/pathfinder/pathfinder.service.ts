import { Injectable } from '@angular/core';
import { Location } from '../../map/location';
import { Creature } from '../../map-object/creature/creature';
import { PathfindingNode } from './pathfinding-node';
import { Vector } from '../../../import/vector/vector';
import { MathService } from '../math/math.service';
import { CanvasGlobalsService } from '../../canvas-globals.service';
import { MapService } from '../../map/map.service';
import { MapObject } from '../../map-object/map-object';
import { CollisionService } from '../../map-object/service/collision.service';

@Injectable({
    providedIn: 'root'
})
export class PathfinderService {
    private closed: PathfindingNode[];
    private creatures: Creature[];
    private end: Location;
    private movingCreature: Creature;
    private open: PathfindingNode[];
    private start: Location;
    private targetCreature;
    private targetNode: PathfindingNode;

    /**
     * Finds a path from an enemy to the player
     * @param canvasGlobals
     * @param mapService
     */
    constructor(
        private canvasGlobals: CanvasGlobalsService,
        private mapService: MapService
    ) {
        this.reset();
    }

    /**
     * Returns a path from moving creature to target creature
     * @param movingCreature
     * @param targetCreature
     * @param creatures
     */
    findPath(
        movingCreature: Creature,
        targetCreature: Creature,
        creatures: Creature[]
    ): PathfindingNode[] {
        let q: PathfindingNode;
        let path;

        // If the creature can't see the target, then just return
        if (MathService.getDistanceBetweenLocations(
               movingCreature.getLocation(),
               targetCreature.getLocation()
            ) > movingCreature.getLineOfSight()) {
            return [];
        }

        this.setup(creatures, movingCreature, targetCreature);
        this.open.push(
            new PathfindingNode(
                this.start,
                this.movingCreature.getWidestDimension(),
                null
            )
        );

        while (this.open.length > 0) {
            this.sortOpenList();
            q = this.open.shift();
            this.checkSuccessors(q, 10, movingCreature.getLineOfSight());
            this.closed.push(q);
        }

        path = this.getPath();
        this.reset();

        return path;
    }

    /**
     * Builds a list of succesors to the passed node
     * @param q
     * @param speed
     */
    private buildSuccessorList(q: PathfindingNode, speed: number) {
        const creatureSize = this.movingCreature.getWidestDimension();
        const successors = [
            new PathfindingNode(new Location(q.location.x - speed, q.location.y - speed), creatureSize, q),
            new PathfindingNode(new Location(q.location.x, q.location.y - speed), creatureSize, q),
            new PathfindingNode(new Location(q.location.x + speed, q.location.y - speed), creatureSize, q),
            new PathfindingNode(new Location(q.location.x - speed, q.location.y), creatureSize, q),
            new PathfindingNode(new Location(q.location.x + speed, q.location.y), creatureSize, q),
            new PathfindingNode(new Location(q.location.x - speed, q.location.y + speed), creatureSize, q),
            new PathfindingNode(new Location(q.location.x, q.location.y + speed), creatureSize, q),
            new PathfindingNode(new Location(q.location.x + speed, q.location.y + speed), creatureSize, q),
        ];

        return this.filterSuccessors(successors);
    }

    /**
     * Calculates the H and G values for the passed node
     * @param node - The node getting values calculated
     * @param q - The parent node needed for calculations
     */
    private calculateHAndGValues(node: PathfindingNode, q: PathfindingNode) {
        node.g = q.g + MathService.getDistanceBetweenLocations(node.location, q.location);
        node.h = MathService.getDistanceBetweenLocations(this.end, node.location);
    }

    /**
     * Builds a list of successors and adds them to the open list if viable
     * @param q
     * @param speed
     * @param lineOfSight
     */
    private checkSuccessors(q: PathfindingNode, speed: number, lineOfSight: number) {
        const successors = this.buildSuccessorList(q, speed);

        successors.some((node: PathfindingNode) => {
            if (this.nodeIsWithinAMove(node, speed)) {
                this.open.length = 0;
                this.targetNode = node;

                return true;
            } else {
                this.calculateHAndGValues(node, q);
                this.nodeSkipCheck(node, lineOfSight);

                if (!node.skip) {
                    this.open.push(node);
                }
            }
        });
    }

    /**
     * Filters out the passed successors that are invalid locations
     * @param successors
     */
    private filterSuccessors(successors: PathfindingNode[]): PathfindingNode[] {
        return successors.filter((node: PathfindingNode) => {
            return this.nodeIsInBounds(node) && !this.nodeWillCollide(node);
        });
    }

    /**
     * Traverses the list of nodes to build a path
     */
    private getPath() {
        const path = [];
        let thisNode: PathfindingNode;

        if (this.targetNode) {
            thisNode = this.targetNode;

            while (thisNode) {
                path.push(thisNode);
                thisNode = thisNode.parent;
            }

            path.reverse().shift();
        }

        return path;
    }

    /**
     * Returns true if the passed node location is within the canvas bounds
     * @param node
     */
    private nodeIsInBounds(node: PathfindingNode): boolean {
            return node.location.x >= 0 &&
            node.location.y >= 0 &&
            node.location.x < this.canvasGlobals.canvasWidth &&
            node.location.y < this.canvasGlobals.canvasHeight;
    }

    /**
     * Returns true if the passed node is within speed of the target
     * @param node
     * @param speed
     */
    private nodeIsWithinAMove(node: PathfindingNode, speed: number): boolean {
        return node.location.x <= (this.end.x + speed) &&
        node.location.x >= (this.end.x - speed) &&
        node.location.y <= (this.end.y + speed) &&
        node.location.y >= (this.end.y - speed);
    }

    /**
     * Sets a node to be skipped if it doesn't meet the criteria
     * @param node
     * @param lineOfSight
     */
    private nodeSkipCheck(node: PathfindingNode, lineOfSight: number) {
        if (this.skipThisNode(node, this.open)) {
            node.skip = true;
        }

        if (this.skipThisNode(node, this.closed)) {
            node.skip = true;
        }

        if (node.getF() > lineOfSight) {
            node.skip = true;
        }
    }

    /**
     * Returns true if the node will collide with something
     */
    private nodeWillCollide(node: PathfindingNode) {
        node.setOwner(this.movingCreature);
        const candidates = this.mapService.getCollisionCandidates(node);

        return candidates.some((candidate) => {
                return candidate !== this.movingCreature &&
                    candidate !== this.targetCreature &&
                    CollisionService.detectCollision(node, candidate);
        });

    }

    /**
     * Resets the service
     */
    private reset() {
        this.closed = [];
        this.end = null;
        this.open = [];
        this.start = null;
        this.targetNode = null;
    }

    /**
     * Rounds the locations of the start and end locations
     */
    private roundLocations(difference: Vector, start: Location, end: Location, prop: string) {
        if (difference[prop] > 0) {
            start[prop] = Math.ceil(start[prop]);
            end[prop] = Math.floor(end[prop]);
        } else {
            start[prop] = Math.floor(start[prop]);
            end[prop] = Math.ceil(end[prop]);
        }
    }

    /**
     * Rounds the start and end locations so we are working with even numbers
     * @param start
     * @param end
     */
    private roundStartAndEndLocations(startCreature: Creature, endCreature: Creature) {
        const start = startCreature.getLocation();
        const end = endCreature.getLocation();
        const startVec = new Vector(start.x, start.y);
        const endVec = new Vector(end.x, end.y);
        const difference = endVec.subtract(startVec);

        this.roundLocations(difference, start, end, 'x');
        this.roundLocations(difference, start, end, 'y');

        this.start = start;
        this.end = end;
    }

    /**
     * Sets the service variables
     * @param creatures
     * @param movingCreature
     * @param targetCreature
     */
    setGlobals(creatures: Creature[], movingCreature: Creature, targetCreature: Creature) {
        this.creatures = creatures;
        this.movingCreature = movingCreature;
        this.targetCreature = targetCreature;
    }


    /**
     * Does all of the setup for A* pathfinding. Sets the class variables,
     * sets up the collision detector, rounds the start and end locations
     * @param creatures
     * @param movingCreature
     * @param targetCreature
     */
    private setup(creatures: Creature[], movingCreature: Creature, targetCreature: Creature) {
        this.setGlobals(creatures, movingCreature, targetCreature);
        this.mapService.setCollisionObjects(this.creatures);
        this.roundStartAndEndLocations(movingCreature, targetCreature);
    }

    /**
     * Checks the passed list for a node with the same location as successorNode and
     * skips it if found and the F value is smaller
     * @param successorNode
     * @param checkList
     */
    private skipThisNode(
            successorNode: PathfindingNode,
            checkList: PathfindingNode[]
        ): boolean {
        let skipNode = false;

        checkList.forEach((checkNode: PathfindingNode, index) => {
            const locationIsSame = successorNode.location.x === checkNode.location.x &&
                successorNode.location.y === checkNode.location.y;
            const listValIsLower = checkNode.getF() < successorNode.getF();

            // If a node with the same location exists in the list and has a
            // lower F val, don't add the new node as there is a better path
            if (locationIsSame && listValIsLower) {
                skipNode = true;
            } else if (locationIsSame && !listValIsLower) {
                // If the new F val is lower, then remove the old one from the
                // list and add the new one
                checkList.splice(index, 1);
            }
        });

        return skipNode;
    }

    /**
     * Returns the node with the lowest F cost from the open array
     */
    private sortOpenList() {
        this.open.sort((a: PathfindingNode, b: PathfindingNode) => {
            return a.getF() - b.getF();
        });
    }
}
