import { Location } from '../../map/location';
import { Vector } from 'src/import/vector/vector';
import { MapEntity } from 'src/app/map-object/map-entity';

export class PathfindingNode extends MapEntity {
    public h: number;
    public g: number;
    public skip: boolean;

    /**
     * A node used to calculate a path in the A* algorithm
     * @param location
     */
    constructor(public location: Location, size: number, public parent: PathfindingNode) {
        super(location, new Vector(1, 0), size);
        this.h = 0;
        this.g = 0;
        this.skip = false;
    }

    /**
     * Returns the f value, which is h + g
     */
    getF() {
        return this.h + this.g;
    }
}
