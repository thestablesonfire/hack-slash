import { Injectable } from '@angular/core';
import {Location} from '../../map/location';
import { MapObject } from 'src/app/map-object/map-object';

@Injectable({
  providedIn: 'root'
})
export class MathService {
    // From https://stackoverflow.com/questions/2259476/rotating-a-point-about-another-point-2d
    static rotatePoint(pivotPoint: Location, pointToRotate: Location, angle: number) {
        const s = Math.sin(angle);
        const c = Math.cos(angle);

        // translate point back to origin:
        pointToRotate.x -= pivotPoint.x;
        pointToRotate.y -= pivotPoint.y;

        // rotate point
        const xNew = pointToRotate.x * c - pointToRotate.y * s;
        const yNew = pointToRotate.x * s + pointToRotate.y * c;

        // translate point back:
        pointToRotate.x = xNew + pivotPoint.x;
        pointToRotate.y = yNew + pivotPoint.y;

        return pointToRotate;
    }

    static getDistanceBetweenObjects(a: MapObject, b: MapObject) {
        return MathService.getDistanceBetweenLocations(a.getLocation(), b.getLocation());
    }

    static getDistanceBetweenLocations(a: Location, b: Location): number {
        const p1 = b.x - a.x;
        const p2 = b.y - a.y;

        return Math.sqrt( p1 * p1 + p2 * p2 );
    }

    static getRandomNumberBetween(min: number, max: number) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}
