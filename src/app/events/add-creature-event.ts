import {Creature} from '../map-object/creature/creature';

export class AddCreatureEvent {
    constructor(public target: Creature) {}
}
