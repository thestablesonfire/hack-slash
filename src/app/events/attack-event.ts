import {Event} from '../input/events/event';
import {MapObject} from '../map-object/map-object';
import {HitBox} from '../map-object/creature/hit-box';

export class AttackEvent extends Event {
    constructor(target: MapObject, protected hitBox: HitBox) {
        super(target);
    }
}
