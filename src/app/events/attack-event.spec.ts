import {Location} from '../map/location';
import {Vector} from '../../import/vector/vector';
import {AttackEvent} from './attack-event';
import {CreatureFactory} from '../map-object/creature/factory/creature.factory';
import {HitBox} from '../map-object/creature/hit-box';

let creatureFactory;
let event;
let goblin;
let hitBox;
let target;

describe('AttackEvent', () => {
    beforeEach(function () {
        creatureFactory = new CreatureFactory();
        goblin = creatureFactory.getGoblin(new Location(30, 30), new Vector(0, -1));
        target = creatureFactory.getPlayer(
            new Location(20, 20),
            new Vector(0, 1)
        );
        hitBox = new HitBox(
            new Location(20, 20),
            new Vector(0, 1),
            20,
            goblin,
            3
        );
        event = new AttackEvent(target, hitBox);
    });

    it('should create the object', () => {
        expect(event).toBeTruthy();
    });
});
