import {fakeAsync, inject, TestBed, tick} from '@angular/core/testing';

import {CanvasDrawerService} from '../../map/canvasDrawer/canvas-drawer.service';
import {EventQueueService} from './event-queue.service';
import {Location} from '../../map/location';
import {MovementAction} from '../../input/events/enums/movement-action.enum';
import {MovementService} from '../../map-object/handlers/movement.service';
import {Vector} from '../../../import/vector/vector';
import {MovementEvent} from '../../input/events/movement-event';
import {MousePositionUpdateEvent} from '../../input/events/mouse-position-update-event';
import {CreatureFactory} from '../../map-object/creature/factory/creature.factory';
import {MouseClickEvent} from '../../input/events/mouse-click-event';
import {MouseButtonTypes} from '../../input/events/enums/mouse-button-types.enum';
import {CombatService} from '../../map-object/handlers/combat.service';
import {DebugService} from '../../services/debug/debug.service';
import {AttackEvent} from '../attack-event';

describe('EventQueueService', () => {
    let creatureFactory;
    let service;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {provide: CanvasDrawerService, useValue: {draw: function () {}}},
                {provide: DebugService, useValue: {printMsg: function () {}}},
                EventQueueService,
                MovementService
            ]
        });
    });

    beforeEach(inject([EventQueueService], (ss) => {
        creatureFactory = new CreatureFactory();
        service = ss;
    }));

    it('should be created', () => {
        const initSpy = spyOn(service, 'init');

        expect(service).toBeTruthy();
        expect(initSpy).toHaveBeenCalledTimes(0);
    });

    it('should start the loop', fakeAsync(() => {
        const canvasSpy = spyOn(TestBed.get(CanvasDrawerService), 'draw');

        service.init();
        tick(19);
        service.stopLoop();

        expect(canvasSpy).toHaveBeenCalledTimes(2);
    }));

    it('should add an event', fakeAsync(() => {
        const movementServiceSpy = spyOn(TestBed.get(MovementService), 'moveCreatures');

        service.addEvent(new MovementEvent(
            creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 1)),
            MovementAction.NONE
        ));

        expect(service.events.length).toBe(1);

        service.init();
        tick(20);
        service.stopLoop();

        expect(service.events.length).toBe(0);
        expect(movementServiceSpy).toHaveBeenCalledTimes(2);
    }));

    it('should call movement service for starts', fakeAsync(() => {
        const movementService  = TestBed.get(MovementService);
        const intervalTime = 17;
        const movementServiceSpyUp = spyOn(movementService, 'startUp');
        const movementServiceSpyDown = spyOn(movementService, 'startDown');
        const movementServiceSpyLeft = spyOn(movementService, 'startLeft');
        const movementServiceSpyRight = spyOn(movementService, 'startRight');

        service.init();
        service.addEvent(new MovementEvent(
            creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 1)),
            MovementAction.START_MOVE_UP
        ));

        tick(intervalTime);

        service.addEvent(new MovementEvent(
            creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 1)),
            MovementAction.START_MOVE_DOWN
        ));

        tick(intervalTime);

        service.addEvent(new MovementEvent(
            creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 1)),
            MovementAction.START_MOVE_LEFT
        ));

        tick(intervalTime);

        service.addEvent(new MovementEvent(
            creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 1)),
            MovementAction.START_MOVE_RIGHT
        ));

        tick(intervalTime);

        service.stopLoop();

        expect(movementServiceSpyUp).toHaveBeenCalledTimes(1);
        expect(movementServiceSpyDown).toHaveBeenCalledTimes(1);
        expect(movementServiceSpyLeft).toHaveBeenCalledTimes(1);
        expect(movementServiceSpyRight).toHaveBeenCalledTimes(1);
    }));

    it('should call movement service for ends', fakeAsync(() => {
        const movementService  = TestBed.get(MovementService);
        const intervalTime = 17;
        const movementServiceSpyUp = spyOn(movementService, 'stopUp');
        const movementServiceSpyDown = spyOn(movementService, 'stopDown');
        const movementServiceSpyLeft = spyOn(movementService, 'stopLeft');
        const movementServiceSpyRight = spyOn(movementService, 'stopRight');

        service.init();
        service.addEvent(new MovementEvent(
            creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 1)),
            MovementAction.END_MOVE_UP
        ));

        tick(intervalTime);

        service.addEvent(new MovementEvent(
            creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 1)),
            MovementAction.END_MOVE_DOWN
        ));

        tick(intervalTime);

        service.addEvent(new MovementEvent(
            creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 1)),
            MovementAction.END_MOVE_LEFT
        ));

        tick(intervalTime);

        service.addEvent(new MovementEvent(
            creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 1)),
            MovementAction.END_MOVE_RIGHT
        ));

        tick(intervalTime);

        service.stopLoop();

        expect(movementServiceSpyUp).toHaveBeenCalledTimes(1);
        expect(movementServiceSpyDown).toHaveBeenCalledTimes(1);
        expect(movementServiceSpyLeft).toHaveBeenCalledTimes(1);
        expect(movementServiceSpyRight).toHaveBeenCalledTimes(1);
    }));

    it('should call movement service for mouse movement', fakeAsync(() => {
        const movementService  = TestBed.get(MovementService);
        const movementServiceMouseSpy = spyOn(movementService, 'updateMousePosition');

        service.handleEvent(
            new MousePositionUpdateEvent(
                creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 1)),
                new Location(0, 0)
            )
        );

        expect(movementServiceMouseSpy).toHaveBeenCalledTimes(1);
    }));

    it('should call movement service for mouse click event', fakeAsync(() => {
        const combatServiceSpy = spyOn(TestBed.get(CombatService), 'initAttack');
        const goblin = creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 0));

        service.init();

        service.addEvent(
            new MouseClickEvent(
                goblin,
                MouseButtonTypes.LEFT_CLICK
            )
        );

        tick(100);

        expect(combatServiceSpy).toHaveBeenCalledTimes(1);
        expect(combatServiceSpy).toHaveBeenCalledWith(goblin);
    }));

    it('should call movement service for attack event', fakeAsync(() => {
        const combatServiceSpy = spyOn(TestBed.get(CombatService), 'initAttack');
        const goblin = creatureFactory.getGoblin(new Location(10, 10), new Vector(1, 0));

        service.init();

        service.addEvent(
            new AttackEvent(
                goblin,
                goblin.attack()
            )
        );

        tick(100);

        expect(combatServiceSpy).toHaveBeenCalledTimes(1);
        expect(combatServiceSpy).toHaveBeenCalledWith(goblin);
    }));
});
