import {Injectable} from '@angular/core';
import {CanvasDrawerService} from '../../map/canvasDrawer/canvas-drawer.service';
import {Event} from '../../input/events/event';
import {MovementService} from '../../map-object/handlers/movement.service';
import {DebugService} from '../../services/debug/debug.service';
import {MovementEvent} from '../../input/events/movement-event';
import {MousePositionUpdateEvent} from '../../input/events/mouse-position-update-event';
import {MouseClickEvent} from '../../input/events/mouse-click-event';
import {CombatService} from '../../map-object/handlers/combat.service';
import {AddCreatureEvent} from '../add-creature-event';
import {AttackEvent} from '../attack-event';
import {MapService} from '../../map/map.service';
import {MapObject} from '../../map-object/map-object';
import { CreatureService } from '../../map-object/handlers/creature.service';
import { Location } from '../../map/location';
import { CanvasGlobalsService } from '../../canvas-globals.service';
import { NestService } from 'src/app/map-object/creature/creature-nest/nest.service';
import { CollisionService } from 'src/app/map-object/service/collision.service';
import { MapEntity } from 'src/app/map-object/map-entity';
import { CreatureNest } from 'src/app/map-object/creature/creature-nest/creature-nest';
import { Creature } from 'src/app/map-object/creature/creature';

@Injectable({
    providedIn: 'root'
})
export class EventQueueService {
    animationRequestId: number;
    cycleCount: number;
    events: Event[];
    loop: number;

    /**
     * Handles all events that are created throughout the execution of the application
     * @param canvasDrawer
     * @param canvasGlobals
     * @param combatService
     * @param creatureService
     * @param debugService
     * @param mapService
     * @param movementService
     */
    constructor(
        private canvasDrawer: CanvasDrawerService,
        private canvasGlobals: CanvasGlobalsService,
        private combatService: CombatService,
        private creatureService: CreatureService,
        private debugService: DebugService,
        private mapService: MapService,
        private movementService: MovementService,
        private nestService: NestService,
    ) {
        this.cycleCount = 0;
        this.events = [];
        this.listenEvents();
    }

    /**
     * Adds an event to the queue
     * @param event
     */
    addEvent(event: Event) {
        this.events.push(event);
    }

    /**
     * Initialization function
     */
    init() {
        this.eventLoop();
    }

    /**
     * Manually starts the event loop
     */
    startLoop() {
        this.eventLoop();
    }

    /**
     * Stops the event processing loop
     */
    stopLoop() {
        cancelAnimationFrame(this.animationRequestId);
    }

    /**
     * Removes all HitBoxes from the MapService
     */
    private clearHitBoxes() {
        this.mapService.clearHitBoxes();
    }

    /**
     * Returns the number of creatures on the map that are owned by owner
     * @param owner
     * @returns {number}
     */
    private creaturesOwnedBy(owner: MapEntity): number {
        const creatures = this.creatureService.getCreatures();
        let count = 0;

        creatures.forEach(function (creature: Creature) {
            if (creature.getOwner() === owner) {
                count++;
            }
        });

        return count;
    }

    /**
     * Calls the CombatService to detect if a hit occurs
     */
    private detectHits() {
        this.combatService.detectHits();
    }

    /**
     * Handles an attack event
     * @param event
     */
    private handleAttackEvent(event: AttackEvent) {
        this.initAttack(event.target);
    }

    /**
     * When an AddCreatureEvent is emitted, it is handled here
     * @param event
     */
    private handleCreatureCreated(event: AddCreatureEvent) {
        const eventCreature = event.target;
        const owner = eventCreature.getOwner();
        let candidates: MapEntity[];

        if (owner instanceof CreatureNest &&
            this.creaturesOwnedBy(owner) < owner.getMaxCreatures()) {
            this.mapService.setCollisionObjects(this.creatureService.getCreatures());
            candidates = this.mapService.getCollisionCandidates(eventCreature);

            const collides = candidates.some(function (candidate: MapEntity) {
                return CollisionService.detectCollision(eventCreature, candidate);
            });

            if (!collides) {
                this.creatureService.addCreature(eventCreature);
            }
        }
    }

    /**
     * Handles the various types of events
     * @param event
     */
    private handleEvent(event: Event) {
        if (event instanceof AttackEvent) {
            this.handleAttackEvent(event);
        } else if (event instanceof MovementEvent) {
            this.handleMovementEvent(event);
        } else if (event instanceof MousePositionUpdateEvent) {
            this.handleMousePositionUpdateEvent(event);
        } else if (event instanceof MouseClickEvent) {
            this.handleMouseClickEvent(event);
        }
    }

    /**
     * Handles a MouseClickEvent
     * @param event
     */
    private handleMouseClickEvent(event: MouseClickEvent) {
        this.debugService.printMsg('Executing event: ' + event.constructor.name + event.button);

        if (!event.target) {
            event.target = this.creatureService.getPlayer();
        }

        this.initAttack(event.target);
    }

    /**
     * Handles a MousePositionUpdateEvent
     * @param event
     */
    private handleMousePositionUpdateEvent(event: MousePositionUpdateEvent) {
        this.debugService.printMsg('Executing event: ' + event.constructor.name + event.position);

        if (!event.target) {
            event.target = this.creatureService.getPlayer();
        }

        this.movementService.updateMousePosition(this.modifyMousePosition(event.position));
    }

    /**
     * Handles a MovementEvent
     * @param event
     */
    private handleMovementEvent(event: MovementEvent) {
        this.debugService.printMsg('Executing event: ' + event.constructor.name + event.action);

        if (!event.target) {
            event.target = this.creatureService.getPlayer();
        }

        this.movementService.directionAction(event.target, event.action);
    }

    /**
     * Initiates an attack from a MapObject
     * @param attacker
     */
    private initAttack(attacker: MapObject) {
        this.combatService.initAttack(attacker);
    }

    /**
     * Subscribes to events
     */
    private listenEvents() {
        this.nestService.creatureCreated.subscribe((event) => {
            this.handleCreatureCreated(event);
        });
    }

    /**
     * The passed in position is relative to the corner of teh screen, not the canvas
     * so we need to remove the distance between the corner and canvas
     */
    private modifyMousePosition(location: Location) {
        const paddingFromLeftEdge = (window.innerWidth - this.canvasGlobals.canvasWidth - (this.canvasGlobals.canvasBorderWidth * 2)) / 2;
        const paddingFromTopEdge = 9; // The margin on the canvas + it's border width (8 + 1)

        location.x = location.x - paddingFromLeftEdge;
        location.y = location.y - paddingFromTopEdge;

        return location;
    }

    /**
     * Updates the creatures' positions
     */
    private moveCreatures() {
        this.movementService.moveCreatures(this.creatureService.getCreatures());
    }

    /**
     * Processes any events in the queue
     */
    private processEvents() {
        while (this.events.length) {
            this.handleEvent(this.events.shift());
        }
    }

    /**
     * Updates the paths of the creatures
     */
    private setPaths() {
        if (!(this.cycleCount % 60)) {
            this.movementService.setPaths(
                this.creatureService.getCreatures(),
                this.mapService.getMapObjects()
            );
        }
    }

    /**
     * Begins the event processing loop
     */
    private eventLoop() {
        this.clearHitBoxes();
        this.processEvents();
        this.detectHits();
        this.setPaths();
        this.moveCreatures();
        this.canvasDrawer.draw(
            this.mapService.getMapObjects(),
            this.creatureService.getCreatures()
        );
        this.cycleCount++;
        this.animationRequestId = requestAnimationFrame(this.eventLoop.bind(this));
    }
}
