import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {InputService} from './input/input.service';
import {CanvasDrawerService} from './map/canvasDrawer/canvas-drawer.service';
import {EventQueueService} from './events/event-queue/event-queue.service';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            providers: [
                {
                    provide: EventQueueService,
                    useValue: {
                        init: function () {},
                        startLoop: function () {},
                    }
                }
            ]
        }).compileComponents();
    }));

    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;

        expect(app).toBeTruthy();
    }));

    it('should set canvas and start event queue', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        const canvasSpy = spyOn(TestBed.get(CanvasDrawerService), 'setCanvas');
        const eventQueueSpy = spyOn(TestBed.get(EventQueueService), 'init');

        app.ngAfterViewInit();

        expect(canvasSpy).toHaveBeenCalledTimes(1);
        expect(eventQueueSpy).toHaveBeenCalledTimes(1);
    }));

    it('should handle the keyUp event', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        const keyUpSpy = spyOn(TestBed.get(InputService), 'handleKeyUpEvent');
        const keyEventUp = new KeyboardEvent('testevent', {
            code: 'KeyW'
        });

        app.keyUp(keyEventUp);

        expect(keyUpSpy).toHaveBeenCalledTimes(1);
        expect(keyUpSpy).toHaveBeenCalledWith(keyEventUp);
    }));

    it('should handle the keyDown event', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        const keyUpSpy = spyOn(TestBed.get(InputService), 'handleKeyDownEvent');
        const keyEventUp = new KeyboardEvent('testevent', {
            code: 'KeyW'
        });

        app.keyDown(keyEventUp);

        expect(keyUpSpy).toHaveBeenCalledTimes(1);
        expect(keyUpSpy).toHaveBeenCalledWith(keyEventUp);
    }));

    it('should handle the mouseClick event', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        const mouseClickSpy = spyOn(TestBed.get(InputService), 'handleMouseClickEvent');
        const clickEvent = new MouseEvent('testevent', {});

        app.mouseClick(clickEvent);

        expect(mouseClickSpy).toHaveBeenCalledTimes(1);
        expect(mouseClickSpy).toHaveBeenCalledWith(clickEvent);
    });

    it('should handle the mouseMove event', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        const mouseMoveSpy = spyOn(TestBed.get(InputService), 'handleMouseMoveEvent');
        const mouseEvent = new MouseEvent('testevent', {
            screenX: 1,
            screenY: 1
        });

        app.mouseMove(mouseEvent);

        expect(mouseMoveSpy).toHaveBeenCalledTimes(1);
        expect(mouseMoveSpy).toHaveBeenCalledWith(mouseEvent);
    }));
});
