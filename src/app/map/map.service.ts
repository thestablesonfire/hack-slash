import { Injectable } from '@angular/core';
import {MapObject} from '../map-object/map-object';
import {Location} from './location';
import {QuadTree} from '../../import/quad-tree/quad-tree';
import {Creature} from '../map-object/creature/creature';
import {BoundingBox} from '../map-object/creature/bounding-box';
import {HitBox} from '../map-object/creature/hit-box';
import {Vector} from '../../import/vector/vector';
import {MapObjectFactory} from '../map-object/service/map-object-factory';
import { CreatureFactory } from '../map-object/creature/factory/creature.factory';
import { NestService } from '../map-object/creature/creature-nest/nest.service';
import { MapEntity } from '../map-object/map-entity';

@Injectable({
    providedIn: 'root'
})
export class MapService {
    private objects: BoundingBox[];
    private quadTree: QuadTree;

    /**
     * Holds all MapObjects (Non-creatures) during the execution of the application
     */
    constructor(creatureFactory: CreatureFactory, private nestService: NestService) {
        const nest = MapObjectFactory.getNest(
                        new Location(600, 600),
                        new Vector(0, 1),
                        creatureFactory.getGoblin(new Location(0, 0), new Vector(0, 1))
                    );
        const up = new Vector(0, -1);

        this.quadTree = new QuadTree({x: 0, y: 0, width: 1400, height: 600}, false);
        this.objects = [];
        this.objects.push(
            MapObjectFactory.getWall(new Location(25, 25), up),
            MapObjectFactory.getWall(new Location(400, 400), up),
            MapObjectFactory.getWall(new Location(400, 100), up),
            MapObjectFactory.getWall(new Location(100, 400), up),
            MapObjectFactory.getWall(new Location(250, 250), up),
            nest
        );

        this.nestService.addNest(nest);
    }

    /**
     * Adds a BoundingBox to the objects array
     * @param mapObject BoundingBox
     */
    addMapObject(mapObject: BoundingBox) {
        this.objects.push(mapObject);
    }

    /**
     * Clears all HitBoxes from the objects array
     */
    clearHitBoxes() {
        this.objects = this.objects.filter((object) => {
            return !(object instanceof HitBox);
        });
    }

    /**
     * Returns the possible collision candidates with the BoundingBox passed
     * @param creature
     */
    getCollisionCandidates(creature: MapEntity): MapObject[] {
        return this.quadTree.retrieve(creature);
    }

    /**
     * Returns all HitBoxes currently on the map
     */
    getHitBoxes(): HitBox[] {
        let hitBoxes;

        hitBoxes = this.objects.filter(function (object: BoundingBox) {
            return object instanceof HitBox;
        });

        return hitBoxes;
    }

    /**
     * Returns all non-creatures on the map
     */
    getMapObjects(): BoundingBox[] {
        return this.objects;
    }

    /**
     * Fills the collision detection with all eligible objects
     * @param creatures
     */
    setCollisionObjects(creatures: Creature[]) {
        this.quadTree.clear();
        this.quadTree.insert(this.objects);
        this.quadTree.insert(creatures);
    }

    /**
     * Removes map objects from this object array
     * @param object
     */
    removeMapObject(object: MapObject) {
        const objectIndex = this.objects.indexOf(object);

        this.objects.splice(objectIndex, 1);
    }
}
