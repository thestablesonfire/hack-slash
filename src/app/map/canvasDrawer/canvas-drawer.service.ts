import {BoundingBox} from '../../map-object/creature/bounding-box';
import {CanvasGlobalsService} from '../../canvas-globals.service';
import {Injectable} from '@angular/core';
import { Creature } from '../../map-object/creature/creature';

@Injectable({
    providedIn: 'root'
})
export class CanvasDrawerService {
    private canvas: HTMLCanvasElement;
    private readonly canvasHeight: number;
    private readonly canvasWidth: number;
    private ctx: CanvasRenderingContext2D;

    /**
     * Draws all objects to the canvas
     * @param canvasGlobals
     */
    constructor(
        private canvasGlobals: CanvasGlobalsService,
    ) {
        this.canvasHeight = this.canvasGlobals.canvasHeight;
        this.canvasWidth = this.canvasGlobals.canvasWidth;
    }

    /**
     * The drawing loop
     */
    draw(mapObjects: BoundingBox[], creatures: Creature[]) {
        if (this.ctx) {
            this.clearCanvas();
            this.drawMapObjects(mapObjects);
            this.drawCreatures(creatures);
        } else {
            console.error('err: no canvas to draw to');
        }
    }

    /**
     * Sets the canvas to be drawn to and sets it's dimensions
     * @param canvas
     */
    setCanvas(canvas: HTMLCanvasElement) {
        if (typeof canvas.getContext === 'function') {
            this.canvas = canvas;
            this.ctx = this.canvas.getContext('2d');
            this.ctx.canvas.height = this.canvasHeight;
            this.ctx.canvas.width = this.canvasWidth;
        } else {
            this.canvas = null;
            this.ctx = null;
        }
    }

    /**
     * Clears the entire canvas
     * TODO: Update this to only erase what is needed
     */
    private clearCanvas() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    /**
     * Draws every creature in the Creature Service
     */
    private drawCreatures(creatures: Creature[]) {
        this.drawObjects(creatures);
    }

    /**
     * Draws all MapObjects from the Map Service
     */
    private drawMapObjects(mapObjects: BoundingBox[]) {
        this.drawObjects(mapObjects);
    }

    /**
     * Draws an array of BoundingBox objects with the `draw` method
     * @param objects BoundingBox[]
     */
    private drawObjects(objects: BoundingBox[]) {
        if (objects) {
            for (const object of objects) {
                object.draw(this.ctx);
            }
        }
    }
}
