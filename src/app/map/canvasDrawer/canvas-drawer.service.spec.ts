import { TestBed, inject } from '@angular/core/testing';

import { CanvasDrawerService } from './canvas-drawer.service';
import {CreatureService} from '../../map-object/handlers/creature.service';
import {Location} from '../location';
import {Vector} from '../../../import/vector/vector';
import {CreatureFactory} from '../../map-object/creature/factory/creature.factory';

describe('CanvasDrawerService', () => {
    let service;
    let canvas;
    let creatureFactory;
    let ctx;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                CanvasDrawerService,
                {provide: CreatureService, useValue: {getCreatures: function() {}}}
            ]
        });

        canvas = document.createElement('canvas');
        creatureFactory = new CreatureFactory();
        ctx = canvas.getContext('2d');
    });

    beforeEach(inject([CanvasDrawerService], (ss) => {
        service = ss;
        service.setCanvas(canvas);
    }));

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should clear canvas then draw map-object', () => {
        const clearCtxSpy = spyOn(ctx, 'clearRect');

        service.draw();

        expect(clearCtxSpy).toHaveBeenCalledTimes(1);
    });

    it('should draw creature', () => {
        const player = creatureFactory.getPlayer(new Location(10, 10), new Vector(0, 1));
        const creature = creatureFactory.getGoblin(new Location(10, 10), new Vector(0, 1));

        const playerDrawSpy = spyOn(player, 'draw');
        const creatureDrawSpy = spyOn(creature, 'draw');

        service.draw([player, creature], []);

        expect(playerDrawSpy).toHaveBeenCalledTimes(1);
        expect(creatureDrawSpy).toHaveBeenCalledTimes(1);
    });

    it('should print an error', () => {
        const consoleSpy = spyOn(console, 'error');
        service.setCanvas({});

        service.draw();

        expect(consoleSpy).toHaveBeenCalledWith('err: no canvas to draw to');
    });
});
