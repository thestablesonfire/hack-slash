/**
 * Center-point of a creature
 */
export class Location {
    constructor(public x: number, public y: number) {}

    clone(): Location {
        return new Location(this.x, this.y);
    }

    moveUp(speed: number) {
        this.y -= speed;
    }

    moveDown(speed: number) {
        this.y += speed;
    }

    moveLeft(speed: number) {
        this.x -= speed;
    }

    moveRight(speed: number) {
        this.x += speed;
    }
}
