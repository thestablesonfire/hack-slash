import {Location} from './location';

let location;

describe('Location', () => {
    beforeEach(function () {
        location = new Location(10, 10);
    });

    it('should create the location', () => {
        expect(location).toBeTruthy();
    });

    it('should move correctly', () => {
        expect(location.x).toBe(10);
        expect(location.y).toBe(10);

        location.moveUp(10);
        expect(location.y).toBe(0);

        location.moveDown(10);
        expect(location.y).toBe(10);

        location.moveLeft(10);
        expect(location.x).toBe(0);

        location.moveRight(10);
        expect(location.x).toBe(10);
    });

    it('should clone itself', () => {
        location.x = 20;
        const clone = location.clone();

        expect(clone.x).toEqual(20);
        expect(clone.y).toEqual(10);
    });
});
