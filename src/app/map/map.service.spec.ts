import { TestBed, inject } from '@angular/core/testing';

import { MapService } from './map.service';
import {Goblin} from '../map-object/creature/types/goblin/goblin';
import {Vector} from '../../import/vector/vector';
import {Location} from './location';
import {HitBox} from '../map-object/creature/hit-box';
import {CreatureService} from '../map-object/handlers/creature.service';

describe('MapService', () => {
    let goblin;
    let service;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [MapService]
        });

        goblin = new Goblin(
            new Location(10, 10),
            new Vector(1, 1),
        );
    });

    beforeEach(inject([MapService], (ss) => {
        service = ss;
    }));

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should add a map object', () => {
        expect(service.getMapObjects().length).toEqual(6);

        service.addMapObject(goblin);

        expect(service.getMapObjects().length).toEqual(7);
    });

    it('should add, get, and clear HitBoxes', () => {
        const hitBox = new HitBox(
            new Location(10, 10),
            new Vector(1, 1),
            20,
            null,
            1
        );

        expect(service.getHitBoxes().length).toEqual(0);

        service.addMapObject(hitBox);

        expect(service.getHitBoxes().length).toEqual(1);

        service.clearHitBoxes();

        expect(service.getHitBoxes().length).toEqual(0);
    });

    it('should call quadTree', () => {
        const quadSpy = spyOn(service.quadTree, 'retrieve');

        service.getCollisionCandidates(goblin);

        expect(quadSpy).toHaveBeenCalledTimes(1);
        expect(quadSpy).toHaveBeenCalledWith(goblin);
    });

    it('should remove mapObject', () => {
        const wall = service.getMapObjects()[0];

        expect(service.getMapObjects().length).toBe(6);

        service.removeMapObject(wall);

        expect(service.getMapObjects().length).toBe(5);

        service.removeMapObject([]);
    });

    it('should add all collision objects', () => {
        const quadClearSpy = spyOn(service.quadTree, 'clear');
        const quadInsertSpy = spyOn(service.quadTree, 'insert');
        const objects = service.getMapObjects();

        service.setCollisionObjects([]);

        expect(quadClearSpy).toHaveBeenCalledTimes(1);
        expect(quadInsertSpy).toHaveBeenCalledTimes(2);
        expect(quadInsertSpy).toHaveBeenCalledWith(objects);
        expect(quadInsertSpy).toHaveBeenCalledWith([]);
    });
});
