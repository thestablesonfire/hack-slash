import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CanvasGlobalsService {
    public canvasBorderWidth: number;
    public canvasHeight: number;
    public canvasWidth: number;

    constructor() {
        this.canvasBorderWidth = 1;
        this.canvasHeight = 768;
        this.canvasWidth = 1024;
    }
}
