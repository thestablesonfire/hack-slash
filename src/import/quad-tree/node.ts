export class Node {
    nodes;
    protected TOP_LEFT = 0;
    protected TOP_RIGHT = 1;
    protected BOTTOM_LEFT = 2;
    protected BOTTOM_RIGHT = 3;
    protected _maxChildren = 4;
    protected _maxDepth = 4;
    protected _depth = 0;
    protected children = null;
    private _bounds = null;

    constructor(bounds, depth, maxDepth, maxChildren) {
        this._bounds = bounds;
        this.children = [];
        this.nodes = [];

        if (maxChildren) {
            this._maxChildren = maxChildren;
        }

        if (maxDepth) {
            this._maxDepth = maxDepth;
        }

        if (depth) {
            this._depth = depth;
        }
    }


    insert(item) {
        if (this.nodes.length) {
            const index = this._findIndex(item);

            this.nodes[index].insert(item);

            return;
        }

        this.children.push(item);

        const  len = this.children.length;

        if (!(this._depth >= this._maxDepth) &&
            len > this._maxChildren) {

            this.subdivide();

            for (let i = 0; i < len; i++) {
                this.insert(this.children[i]);
            }

            this.children.length = 0;
        }
    }

    retrieve(item) {
        if (this.nodes.length) {
            const index = this._findIndex(item);

            return this.nodes[index].retrieve(item);
        }

        return this.children;
    }

    _findIndex(item) {
        const b = this._bounds;
        const left = (item.getLocation().x > b.x + b.width / 2) ? false : true;
        const top = (item.getLocation().y > b.y + b.height / 2) ? false : true;

        // top left
        let index = this.TOP_LEFT;
        if (left) {
            // left side
            if (!top) {
                // bottom left
                index = this.BOTTOM_LEFT;
            }
        } else {
            // right side
            if (top) {
                // top right
                index = this.TOP_RIGHT;
            } else {
                // bottom right
                index = this.BOTTOM_RIGHT;
            }
        }

        return index;
    }


    subdivide() {
        const depth = this._depth + 1;

        const bx = this._bounds.x;
        const by = this._bounds.y;

        // floor the values
        const b_w_h = (this._bounds.width / 2); // todo: Math.floor?
        const b_h_h = (this._bounds.height / 2);
        const bx_b_w_h = bx + b_w_h;
        const by_b_h_h = by + b_h_h;

        // top left
        this.nodes[this.TOP_LEFT] = new Node({
                x: bx,
                y: by,
                width: b_w_h,
                height: b_h_h
            },
            depth, this._maxDepth, this._maxChildren);

        // top right
        this.nodes[this.TOP_RIGHT] = new Node({
                x: bx_b_w_h,
                y: by,
                width: b_w_h,
                height: b_h_h
            },
            depth, this._maxDepth, this._maxChildren);

        // bottom left
        this.nodes[this.BOTTOM_LEFT] = new Node({
                x: bx,
                y: by_b_h_h,
                width: b_w_h,
                height: b_h_h
            },
            depth, this._maxDepth, this._maxChildren);


        // bottom right
        this.nodes[this.BOTTOM_RIGHT] = new Node({
                x: bx_b_w_h,
                y: by_b_h_h,
                width: b_w_h,
                height: b_h_h
            },
            depth, this._maxDepth, this._maxChildren);
    }

    clear() {
        this.children.length = 0;

        const len = this.nodes.length;

        for (let i = 0; i < len; i++) {
            this.nodes[i].clear();
        }

        this.nodes.length = 0;
    }
}
