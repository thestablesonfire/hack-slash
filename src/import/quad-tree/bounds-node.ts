import {Node} from './node';

export class BoundsNode extends Node {
    _out: BoundsNode[];
    _stuckChildren: BoundsNode[];

    constructor(bounds, depth, maxChildren, maxDepth) {
        super(bounds, depth, maxChildren, maxDepth);
        this._stuckChildren = [];
        this._out = [];
    }

    insert(item) {
        if (this.nodes.length) {
            const index = this._findIndex(item);
            const node = this.nodes[index];

            // todo: make _bounds bounds
            if (item.getLocation().x >= node._bounds.x &&
                item.getLocation().x + item.getSize() <= node._bounds.x + node._bounds.width &&
                item.getLocation().y >= node._bounds.y &&
                item.getLocation().y + item.getSize() <= node._bounds.y + node._bounds.height) {

                this.nodes[index].insert(item);

            } else {
                this._stuckChildren.push(item);
            }

            return;
        }

        this.children.push(item);

        const len = this.children.length;

        if (!(this._depth >= this._maxDepth) &&
            len > this._maxChildren) {

            this.subdivide();

            for (let i = 0; i < len; i++) {
                this.insert(this.children[i]);
            }

            this.children.length = 0;
        }
    }

    getChildren() {
        return this.children.concat(this._stuckChildren);
    }

    retrieve(item) {
        const out = this._out;
        out.length = 0;

        if (this.nodes.length) {
            const index = this._findIndex(item);
            const node = this.nodes[index];

            if (item.getLocation().x >= node._bounds.x &&
                item.getLocation().x + item.getSize() <= node._bounds.x + node._bounds.width &&
                item.getLocation().y >= node._bounds.y &&
                item.getLocation().y + item.getSize() <= node._bounds.y + node._bounds.height) {

                out.push.apply(out, this.nodes[index].retrieve(item));
            } else {
                // Part of the item are overlapping multiple child nodes. For each of the overlapping nodes, return all containing objects.

                if (item.getLocation().x <= this.nodes[this.TOP_RIGHT]._bounds.x) {
                    if (item.getLocation().y <= this.nodes[this.BOTTOM_LEFT]._bounds.y) {
                        if (typeof this.nodes[this.TOP_LEFT].getAllContent === 'function') {
                            out.push.apply(out, this.nodes[this.TOP_LEFT].getAllContent());
                        }
                    }

                    if (item.getLocation().y + item.getSize() > this.nodes[this.BOTTOM_LEFT]._bounds.y) {
                        if (typeof this.nodes[this.BOTTOM_LEFT].getAllContent === 'function') {
                            out.push.apply(out, this.nodes[this.BOTTOM_LEFT].getAllContent());
                        }
                    }
                }

                if (item.getLocation().x + item.getSize() > this.nodes[this.TOP_RIGHT]._bounds.x) {// position+width bigger than middle x
                    if (item.getLocation().y <= this.nodes[this.BOTTOM_RIGHT]._bounds.y) {
                        if (typeof this.nodes[this.TOP_RIGHT].getAllContent === 'function') {
                            out.push.apply(out, this.nodes[this.TOP_RIGHT].getAllContent());
                        }
                    }

                    if (item.getLocation().y + item.getSize() > this.nodes[this.BOTTOM_RIGHT]._bounds.y) {
                        if (typeof this.nodes[this.BOTTOM_RIGHT].getAllContent === 'function') {
                            out.push.apply(out, this.nodes[this.BOTTOM_RIGHT].getAllContent());
                        }
                    }
                }
            }
        }

        out.push.apply(out, this._stuckChildren);
        out.push.apply(out, this.children);

        return out;
    }

    // Returns all contents of node.
    getAllContent() {
        const out = this._out;

        if (this.nodes.length) {

            for (let i = 0; i < this.nodes.length; i++) {
                this.nodes[i].getAllContent();
            }
        }
        out.push.apply(out, this._stuckChildren);
        out.push.apply(out, this.children);
        return out;
    }

    clear() {

        this._stuckChildren.length = 0;

        // array
        this.children.length = 0;

        const len = this.nodes.length;

        if (!len) {
            return;
        }

        for (let i = 0; i < len; i++) {
            this.nodes[i].clear();
        }

        // array
        this.nodes.length = 0;

        // we could call the super clear function but for now, im just going to inline it
        // call the hidden super.clear, and make sure its called with this = this instance
        // Object.getPrototypeOf(BoundsNode.prototype).clear.call(this);
    }
}
